using System.Collections.Generic;
using Flurl.Http;
using MicroservicesCommon.Common;

namespace MicroservicesCommon.HttpServices
{
    public static class FlurlHttpExtension
    {
        public static FlurlClient AddHeader(this FlurlClient flurlClient, IDictionary<string, string> header)
        {
            if (header != null)
            {
                foreach (var head in header)
                {
                    flurlClient.Headers.AddOrReplace(head.Key, head.Value);
                }
            }

            return flurlClient;
        }
    }
}