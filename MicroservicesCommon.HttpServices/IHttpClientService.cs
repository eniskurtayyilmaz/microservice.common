using System.Collections.Generic;
using Flurl.Http;
using MicroservicesCommon.Common;
using MicroservicesCommon.Services;
using Microsoft.Extensions.Configuration;

namespace MicroservicesCommon.HttpServices
{
    public interface IHttpClientService
    {
        public FlurlClient GetClient(string configKey);
    }

    public class HttpClientService : IHttpClientService
    {
        private readonly IConfiguration _configuration;
        private readonly IUserResolverService _userResolverService;
        private readonly ITraceIdResolverService _traceIdResolverService;

        public HttpClientService(IConfiguration configuration, IUserResolverService userResolverService,
            ITraceIdResolverService traceIdResolverService)
        {
            _configuration = configuration;
            _userResolverService = userResolverService;
            _traceIdResolverService = traceIdResolverService;
        }

        public FlurlClient GetClient(string configKey)
        {
            var baseUrl = _configuration[configKey];
            var cli = new FlurlClient(baseUrl);
            cli.WithTimeout(60);
            cli.Headers.Add(Constants.UserIdHeaderName, _userResolverService.GetCurrentUser());
            cli.Headers.Add(Constants.BeymenUserEmailHeaderName, _userResolverService.GetBeymenUserEmail());
            cli.Headers.Add(Constants.RoleIdHeaderName, _userResolverService.GetCurrentRole());
            cli.Headers.Add(Constants.TraceIdHeaderName, _traceIdResolverService.GetTraceId());
            cli.Headers.Add(Constants.RequestProjectHeaderName, _userResolverService.GetCurrentRequestProject());
            return cli;
        }
    }
}