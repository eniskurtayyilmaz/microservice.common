using System.Linq;
using Flurl.Http;
using MicroservicesCommon.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace MicroservicesCommon.HttpServices
{
    public static class DependencyInjection
    {
        public static void AddHttpClientService(this IServiceCollection services)
        {
            services.AddSingleton<IHttpClientService, HttpClientService>();

            FlurlHttp.Configure(settings =>
            {
                var builder = services.BuildServiceProvider();
                var logger = builder.GetService<ILogger<IHttpClientService>>();
                settings.BeforeCall = call =>
                {
                    var headers = call.Request.Headers.Select(p => new
                    {
                        Key = p.Name,
                        Value = p.Value
                    });
                    logger.LogInformation("Http Request Created => {context} *** {header} *** {body}",
                        call,
                        JsonConvert.SerializeObject(headers),
                        call.RequestBody);
                };

                settings.AfterCall = async call =>
                {
                    var headers = call.Request.Headers.Select(p => new
                    {
                        Key = p.Name,
                        Value = p.Value
                    });
                    
                    var statusCode = call.Response.StatusCode;
                    
                    if (!statusCode.ToString().StartsWith("20"))
                    {
                        var message = await call.Response.GetStringAsync();
                        logger.LogError("Http Request Has Error => {context} *** {message} *** {header} *** {body}",
                            call,
                            message,
                            JsonConvert.SerializeObject(headers),
                            call.RequestBody);
                    }
                };

                settings.OnError = async call =>
                {
                    var headers = call.Request.Headers.Select(p => new
                    {
                        Key = p.Name,
                        Value = p.Value
                    });

                    var message = await call.Response.GetStringAsync();

                    logger.LogError("Http Request Has Error => {context} *** {message} *** {header} *** {body}",
                        call,
                        message,
                        JsonConvert.SerializeObject(headers),
                        call.RequestBody);
                };
            });
        }
    }
}