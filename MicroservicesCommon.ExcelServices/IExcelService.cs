using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using MicroservicesCommon.ExcelServices.Attributes;
using MicroservicesCommon.ExcelServices.Extensions;
using MicroservicesCommon.ExcelServices.Models;
using MicroservicesCommon.Utils;
using Microsoft.Extensions.Configuration;

namespace MicroservicesCommon.ExcelServices
{
    public interface IExcelService
    {
        public void GenerateExcel<T>(IEnumerable<T> data, MemoryStream memoryStream)
            where T : IExcelSheet, new();

        public void GenerateTemplateExcel<T>(T data, List<ExcelAttribute> dropDownItems, MemoryStream memoryStream, IXLWorksheet mergeSheet)
            where T : IExcelSheet, new();

        public ExcelConvertResponse<T> GetExcelConvertResponse<T>(Stream stream) where T : IExcelSheet, new();
        public ExcelConvertResponse<T> GenerateTemplateConvertResult<T>(Stream stream) where T : IExcelSheet, new();
        public IXLWorksheet GetXLWorksheetFromStream(Stream stream, string sheetName);
    }

    public class ExcelService : IExcelService
    {
        private readonly IConfiguration _configuration;
        public ExcelService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void GenerateExcel<T>(IEnumerable<T> data, MemoryStream memoryStream)
            where T : IExcelSheet, new()
        {
            using (var workbook = new XLWorkbook(XLEventTracking.Disabled))
            {
                var worksheet = workbook.Worksheets.Add(typeof(T).GetSheetName());
                CreateHeader<T>(worksheet);
                foreach (var item in data)
                {
                    var t = item.GetType();
                    foreach (var propertyInfo in t.GetProperties())
                    {
                        var propertyType = propertyInfo.PropertyType;
                        if (propertyType == typeof(DateTime))
                        {
                            var value = propertyInfo.GetValue(item);
                            if (value != null)
                            {
                                var systemTimeZoneById = TimeZoneInfo.FindSystemTimeZoneById(_configuration["ZoneId"]);
                                var dateTime = TimeZoneInfo.ConvertTime(Convert.ToDateTime(value), systemTimeZoneById);
                                propertyInfo.SetValue(item, dateTime);
                            }
                        }
                    }
                }
                
                worksheet.Cell(2, 1).InsertData(data);

                var model = typeof(T);
                var propertiesCount = model.GetProperties().Count();
                int dataCount = data.Count() + 1;
                worksheet.Range(1, 1, dataCount, propertiesCount).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Range(1, 1, dataCount, propertiesCount).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                worksheet.Range(1, 1, dataCount, propertiesCount).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                worksheet.Range(1, 1, dataCount, propertiesCount).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                worksheet.Rows(2, data.Count() + 1).Style.Font.SetFontSize(12);
                worksheet.Rows(2, data.Count() + 1).Height = 15.75;
                

                workbook.SaveAs(memoryStream);
            }
        }

        public ExcelConvertResponse<T> GetExcelConvertResponse<T>(Stream stream) where T : IExcelSheet, new()
        {
            var excelConvertResponseData = new ExcelConvertResponse<T>();

            try
            {
                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(typeof(T).GetSheetName());
                    var headerRow = worksheet.Row(1);

                    var rows = worksheet.RangeUsed().RowsUsed().Skip(1); // Skip header row

                    var props = typeof(T).GetProperties();

                    excelConvertResponseData.Titles =
                        headerRow.Cells().Take(props.Length).Select(p => p.GetValue<string>()).ToArray();

                    foreach (var row in rows)
                    {
                        var instance = new T();
                        for (int i = 0; i < props.Length; i++)
                        {
                            var prop = props[i];
                            var excelValue = row.Cell(i + 1).GetValue<object>();
                            var value = Convert.ChangeType(excelValue, prop.PropertyType);
                            prop.SetValue(instance, value, null);
                        }

                        excelConvertResponseData.Items.Add(instance);
                    }
                }

                return excelConvertResponseData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return excelConvertResponseData;
            }
        }

        private void CreateHeader<T>(IXLWorksheet worksheet) where T : IExcelSheet, new()
        {
            var model = typeof(T);
            var properties = model.GetProperties()
                .Where(p => p.CustomAttributes.Any(x => x.AttributeType == typeof(ExcelColumnAttribute)))
                .ToList();

            if (!properties.Any())
            {
                throw new ArgumentException("Column does not exists.");
            }

            for (int i = 1; i <= properties.Count; i++)
            {
                var property = properties[i - 1];
                var attributes = property.GetCustomAttributes(typeof(object), true);
                var width = ((ExcelColumnAttribute)attributes[0]).Width;
                var name = ((ExcelColumnAttribute)attributes[0]).Name;
                var title = ((ExcelColumnAttribute)attributes[0]).Title;
                var isHide = ((ExcelColumnAttribute)attributes[0]).IsHide;
                worksheet.Cell(1, i).SetValue(name);
                worksheet.Columns(i.ToString()).Width = width;

                if (isHide)
                {
                    worksheet.Columns(i.ToString()).Hide();
                }
                if (!string.IsNullOrEmpty(title))
                {
                    var message = ((ExcelColumnAttribute)attributes[0]).Message;
                    var range = worksheet.Range(1, i, 10000, i).SetDataValidation();
                    range.InputTitle = title;
                    range.InputMessage = message;
                }

            }

            worksheet.Row(1).CellsUsed().Style.Fill.SetBackgroundColor(XLColor.FromArgb(89, 89, 89));
            worksheet.Row(1).CellsUsed().Style.Font.FontColor = XLColor.White;
            worksheet.Row(1).Height = 45;
            worksheet.Row(1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet.Row(1).Style.Alignment.WrapText = true;

            if (model.IsHaveDescription())
            {
                var description = model.GetDescription();
                worksheet.Cell(1, properties.Count + 2).SetValue(description.Name);
                worksheet.Columns((properties.Count + 2).ToString()).Width = description.Width;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Fill
                    .SetBackgroundColor(XLColor.FromArgb(89, 89, 89));
                worksheet.Row(1).Cell(properties.Count + 2).Style
                    .Font.FontColor = XLColor.White;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Alignment.Horizontal =
                    XLAlignmentHorizontalValues.Left;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Alignment.Vertical =
                    XLAlignmentVerticalValues.Center;

                worksheet.Columns((properties.Count + 1).ToString()).Width = 2;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).Cell(properties.Count + 2).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
            }
            worksheet.Row(1).CellsUsed().Style.Font.SetBold();
            worksheet.Row(1).CellsUsed().Style.Font.SetFontSize(11);
        }

        public void GenerateTemplateExcel<T>(T data, List<ExcelAttribute> dropDownItems, MemoryStream memoryStream, IXLWorksheet? mergeSheet) where T : IExcelSheet, new()
        {
            using (var workbook = new XLWorkbook(XLEventTracking.Disabled))
            {
                var worksheet = workbook.Worksheets.Add(typeof(T).GetSheetName());

                var attributesSheet = workbook.Worksheets.Add("Urun Detay Bilgileri");
                var attributeOptions = workbook.Worksheets.Add("AttributeOptions");

                attributesSheet.Protect(RandomUtils.RandomAlphaNumeric(10))
                    .AllowElement(XLSheetProtectionElements.FormatCells)
                    .AllowElement(XLSheetProtectionElements.FormatColumns)
                    .AllowElement(XLSheetProtectionElements.FormatRows);

                attributeOptions.Protect(RandomUtils.RandomAlphaNumeric(10));


                CreateHeader<T>(worksheet); // sabit headerlar olu�acak

                var model = typeof(T);
                var properties = model.GetProperties()
                    .Where(p => p.CustomAttributes.Any(x => x.AttributeType == typeof(ExcelColumnAttribute)))
                    .ToList();

                if (!properties.Any())
                {
                    throw new ArgumentException("Column does not exists.");
                }

                for (int i = 1; i <= properties.Count; i++)
                {
                    var property = properties[i - 1];
                    var attributes = property.GetCustomAttributes(typeof(object), true);
                    var name = ((ExcelColumnAttribute)attributes[0]).Name;
                    var isRequired = ((ExcelColumnAttribute)attributes[0]).IsRequired;
                    var isProtectedColumn = ((ExcelColumnAttribute)attributes[0]).ProtectedColumn;
                    var column = worksheet.GetColumn(name);
                    if (!isRequired)
                    {
                        column.FirstCell().Style.Fill.SetBackgroundColor(XLColor.White);
                        column.FirstCell().Style.Font.FontColor = XLColor.Black;
                    }
                    if (isProtectedColumn)
                    {
                        var value = property.GetValue(data, null);
                        if (value != null)
                        {
                            column.FirstCell().CellBelow().Value = value;
                            column.FirstCell().CellBelow().Style.Protection.SetLocked(true);
                            workbook.CustomProperties.Add(name, value);
                        }
                    }
                }

                foreach (var item in dropDownItems)
                {
                    if (item.IsFreeText)
                    {
                        AddFreeTextColumn(worksheet, item.Value);
                        AddDrowdownItemWithHeader(attributeOptions, item.Value, item.AttributeOptions.Select(a => a.Value).ToList());
                        AddDrowdownItemWithHeader(attributeOptions, item.Id, item.AttributeOptions.Select(a => a.Id).ToList());
                    }
                    else
                    {
                        var column = worksheet.GetColumn(item.Value);
                        if (column == null)
                        {
                            worksheet.AddHeader(item.Value);
                        }

                        AddDrowdownItemWithHeader(attributesSheet, item.Value, item.AttributeOptions.Select(a => a.Value).ToList());
                        AddDrowdownItemWithHeader(attributeOptions, item.Value, item.AttributeOptions.Select(a => a.Value).ToList());
                        AddDrowdownItemWithHeader(attributeOptions, item.Id, item.AttributeOptions.Select(a => a.Id).ToList());
                        AddDrownDownItems(worksheet, attributesSheet, item.Value);
                    }
                }

                worksheet.RowsUsed().Height = 15;

                worksheet.ApplyHeaderStyle();
                attributesSheet.ApplyHeaderStyle();
                attributesSheet.ApplyColorStyle();
                attributesSheet.ApplyUsedCellBorderStyle();
                attributeOptions.ApplyHeaderStyle();
                attributeOptions.ApplyColorStyle();
                attributeOptions.ApplyUsedCellBorderStyle();
                attributeOptions.Hide();

                worksheet.Row(1).CellsUsed().Style.Border.TopBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).CellsUsed().Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).CellsUsed().Style.Border.RightBorder = XLBorderStyleValues.Thin;
                worksheet.Row(1).CellsUsed().Style.Border.BottomBorder = XLBorderStyleValues.Thin;
                

                if (mergeSheet != null)
                {
                    workbook.AddWorksheet(mergeSheet);
                    workbook.Worksheet(mergeSheet.Name).ShowGridLines = false;
                    workbook.Worksheet(mergeSheet.Name).Protect(RandomUtils.RandomAlphaNumeric(10));
                }

                workbook.SaveAs(memoryStream);
            }
        }

        public ExcelConvertResponse<T> GenerateTemplateConvertResult<T>(Stream stream) where T : IExcelSheet, new()
        {
            var excelConvertResponseData = new ExcelConvertResponse<T>();

            try
            {
                using (var workbook = new XLWorkbook(stream))
                {
                    var worksheet = workbook.Worksheet(typeof(T).GetSheetName());
                    var attributesSheet = workbook.Worksheet("Urun Detay Bilgileri");
                    var attributeOptions = workbook.Worksheet("AttributeOptions");
                    var headerRow = worksheet.Row(1);

                    var rows = worksheet.RangeUsed().RowsUsed().Skip(1); // Skip header row

                    var props = typeof(T).GetProperties();

                    excelConvertResponseData.Titles = headerRow.CellsUsed().Select(p => p.GetValue<string>()).ToArray();
                    foreach (var row in rows)
                    {
                        var instance = new T();
                        for (int i = 0; i < props.Length - 1; i++)
                        {
                            var prop = props[i];
                            var excelValue = row.Cell(i + 1).GetValue<object>();

                            var value = Convert.ChangeType(excelValue, prop.PropertyType);
                            prop.SetValue(instance, value, null);
                        }

                        List<ExcelAttributeParse> attributes = new List<ExcelAttributeParse>();
                        for (int i = props.Length - 1; i < excelConvertResponseData.Titles.Length; i++)
                        {
                            var header = headerRow.Cell(i + 1).GetValue<string>();
                            var excelValue = row.Cell(i + 1).GetValue<string>();
                            if (!string.IsNullOrEmpty(excelValue))
                            {
                                var column = attributeOptions.GetColumn(header);
                                var attributesId = column.FirstCell().CellRight().GetString();
                                var attributeOptionId = column.CellsUsed().Where(a => a.Value.ToString() == excelValue).First().CellRight().GetString();

                                var attribute = new ExcelAttributeParse
                                {
                                    Id = attributesId,
                                    Value = header,
                                    AttributeOptions = new ExcelAttributeOption
                                    {
                                        Id = attributeOptionId,
                                        Value = excelValue,
                                    }
                                };
                                attributes.Add(attribute);
                            }

                        }

                        var customProductAttribute = props.Where(p => p.CustomAttributes.Any(x => x.AttributeType == typeof(ExcelProductAttAttribute))).FirstOrDefault();
                        if (customProductAttribute != null)
                        {
                            var value = Convert.ChangeType(attributes, customProductAttribute.PropertyType);
                            customProductAttribute.SetValue(instance, value, null);
                        }

                        excelConvertResponseData.Items.Add(instance);
                    }
                }

                return excelConvertResponseData;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return excelConvertResponseData;
            }
        }
        private static void AddDrowdownItemWithHeader(IXLWorksheet worksheet, string header, List<string> items)
        {
            var range = worksheet.RangeUsed();
            
            if (range == null)
            {
                worksheet.AddHeader(header);
                worksheet.Cell(2, 1).InsertData(items);
            }
            else
            {
                worksheet.Cell(1, range.ColumnCount() + 1).Value = header;
                worksheet.Cell(2, range.ColumnCount() + 1).InsertData(items);
            }
        }

        private static void AddFreeTextColumn(IXLWorksheet worksheet, string header)
        {
            var range = worksheet.RangeUsed();
            if (range == null)
            {
                worksheet.AddHeader(header);
            }
            else
            {
                worksheet.Cell(1, range.ColumnCount() + 1).Value = header;
            }
        }

        private static void AddDrownDownItems(IXLWorksheet worksheet, IXLWorksheet dataValidationSheet, string header)
        {
            var validationColumn = dataValidationSheet.GetColumn(header);
            var itemsRangeWithoutHeader = dataValidationSheet.Range(validationColumn.FirstCell().CellBelow(), validationColumn.LastCellUsed());

            var wsColumn = worksheet.GetColumn(header);
            worksheet.Range(2, wsColumn.ColumnNumber(), 10000, wsColumn.ColumnNumber()).SetDataValidation().List(itemsRangeWithoutHeader, true);
        }

        public IXLWorksheet GetXLWorksheetFromStream(Stream ms, string sheetName)
        {
            var workbook = new XLWorkbook(ms);
            workbook.TryGetWorksheet(sheetName, out IXLWorksheet worksheet);
            return worksheet;
        }
    }
}