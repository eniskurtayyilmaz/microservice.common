using Microsoft.Extensions.DependencyInjection;

namespace MicroservicesCommon.ExcelServices
{
    public static class DependencyInjection
    {
        public static void AddExcelService(this IServiceCollection services)
        {
            services.AddSingleton<IExcelService, ExcelServices.ExcelService>();
        }
    }
}