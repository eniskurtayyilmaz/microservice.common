﻿using System.Collections.Generic;

namespace MicroservicesCommon.ExcelServices.Models
{
    public class ExcelConvertResponse<T>
    {
        public ExcelConvertResponse()
        {
            Items = new List<T>();
        }

        public string[] Titles { get; set; }
        public List<T> Items { get; set; }
    }
}