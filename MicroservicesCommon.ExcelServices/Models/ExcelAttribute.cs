﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroservicesCommon.ExcelServices.Models
{
    public class ExcelAttribute
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public bool IsFreeText { get; set; }
        public List<ExcelAttributeOption> AttributeOptions { get; set; }
    }

    public class ExcelAttributeOption
    {
        public string Id { get; set; }
        public string Value { get; set; }
    }

    public class ExcelAttributeParse
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public ExcelAttributeOption AttributeOptions { get; set; }
    }
}
