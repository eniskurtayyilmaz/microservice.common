using System;
using System.Linq;
using ClosedXML.Excel;
using MicroservicesCommon.ExcelServices.Attributes;
using MicroservicesCommon.ExcelServices.Models;

namespace MicroservicesCommon.ExcelServices.Extensions
{
    public static class ExcelSheetExtensions
    {
        public static string GetSheetName<T>(this T obj) where T : IExcelSheet, new()
        {
            var attributes = typeof(T).GetCustomAttributes(typeof(object), true);
            if (!attributes.Any())
            {
                throw new ArgumentException("SheetName does not exists.");
            }
            return ((ExcelSheetNameAttribute)attributes[0]).Name;
        }

        public static string GetSheetName(this Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(object), true);
            if (!type.IsHaveSheetName())
            {
                throw new ArgumentException("SheetName does not exists.");
            }
            return (attributes.FirstOrDefault(x => x is ExcelSheetNameAttribute) as ExcelSheetNameAttribute)?.Name;
        }

        public static bool IsHaveSheetName(this Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(object), true);
            return attributes.Any(x => x is ExcelSheetNameAttribute);
        }

        public static void AddHeader(this IXLWorksheet worksheet, string header)
        {
            var range = worksheet.RangeUsed();
            if (range == null)
            {
                worksheet.Cell(1, 1).Value = header;
            }
            else
            {
                worksheet.Cell(1, range.ColumnCount() + 1).Value = header;
            }
        }

        public static IXLColumn GetColumn(this IXLWorksheet worksheet, string header)
        {
            return worksheet.RangeUsed().Cells().Where(a => a.Value.ToString() == header).FirstOrDefault()?.WorksheetColumn();
        }
        public static IXLColumn GetColumn2(this IXLWorksheet xLWorksheet, string header)
        {
            var column = xLWorksheet.Row(1).CellsUsed().Select((a, index) => new
            {
                index = index,
                header = a.Value.ToString()
            }).Where(s => s.header.Contains(header)).FirstOrDefault();

            return xLWorksheet.Column(column.index);
        }

        public static void ApplyHeaderStyle(this IXLWorksheet worksheet)
        {
            worksheet.Row(1).Height = 45;
            worksheet.Row(1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            worksheet.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            worksheet.Row(1).Style.Alignment.WrapText = true;
            worksheet.Row(1).CellsUsed().Style.Font.SetBold();
            worksheet.Row(1).CellsUsed().Style.Font.SetFontSize(11);

            worksheet.ColumnsUsed().Where(a => a.Width < 12).ToList().ForEach(a => a.Width = 12);
        }


        public static void ApplyColorStyle(this IXLWorksheet worksheet)
        {
            worksheet.Row(1).CellsUsed().Style.Fill.SetBackgroundColor(XLColor.FromArgb(89, 89, 89));
            worksheet.Row(1).CellsUsed().Style.Font.FontColor = XLColor.White;
        }

        public static void ApplyUsedCellBorderStyle(this IXLWorksheet worksheet)
        {
            worksheet.RowsUsed().CellsUsed().Style.Border.TopBorder = XLBorderStyleValues.Thin;
            worksheet.RowsUsed().CellsUsed().Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            worksheet.RowsUsed().CellsUsed().Style.Border.RightBorder = XLBorderStyleValues.Thin;
            worksheet.RowsUsed().CellsUsed().Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        }
    }
}