using System;
using System.Linq;
using MicroservicesCommon.ExcelServices.Attributes;

namespace MicroservicesCommon.ExcelServices.Extensions
{
    public static class ExcelDescriptionColumnAttributeExtensions
    {
        public static bool IsHaveDescription(this Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(object), true);
            return attributes.Any(x => x is ExcelDescriptionColumnAttribute);
        }

        public static ExcelDescriptionColumnAttribute GetDescription(this Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(object), true);
            
            if (!type.IsHaveDescription())
            {
                throw new ArgumentException("Descrtiption does not exists.");
            }

            return (attributes.FirstOrDefault(x => x is ExcelDescriptionColumnAttribute) as ExcelDescriptionColumnAttribute);
        }
    }
}