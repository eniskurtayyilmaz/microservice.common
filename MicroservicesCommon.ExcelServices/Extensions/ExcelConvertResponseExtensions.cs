using System.Linq;
using MicroservicesCommon.ExcelServices.Attributes;
using MicroservicesCommon.ExcelServices.Models;

namespace MicroservicesCommon.ExcelServices.Extensions
{
    public static class ExcelConvertResponseExtensions
    {
        public static (bool isValid, string errorMessage) IsValid<T>(this string[] comparingTitles)
            where T : IExcelSheet, new()
        {
            var model = typeof(T);
            var properties = model.GetProperties()
                .Where(p => p.CustomAttributes.Any(x => x.AttributeType == typeof(ExcelColumnAttribute)))
                .ToList();

            if ((comparingTitles == null || !comparingTitles.Any()) || !properties.Any() ||
                comparingTitles.Length != properties.Count)
            {
                return (false, "Exceldeki kolon başlıkları ve yerleri değiştirilemez.");
            }

            for (int i = 1; i <= properties.Count; i++)
            {
                var property = properties[i - 1];
                var attributes = property.GetCustomAttributes(typeof(object), true);

                var title = ((ExcelColumnAttribute) attributes[0]).Name;
                if (!comparingTitles.Contains(title))
                {
                    return (false, "Dosya içerisinde data bulunamadı.");
                }
            }

            return (true, string.Empty);
        }
    }
}