using MicroservicesCommon.ExcelServices.Extensions;
using System;

namespace MicroservicesCommon.ExcelServices.Attributes
{
    public class ExcelColumnAttribute : Attribute
    {
        public ExcelColumnAttribute(string name, double width = 12, bool isRequired = true, bool protectedColumn = false, bool isSelected = false, Type resourceType = null,
            string titleResourceName = "",
            string messageResourceName = "",
            bool isHide = false)
        {
            Name = name;
            Width = width;
            IsRequired = isRequired;
            ProtectedColumn = protectedColumn;
            IsSelected = isSelected;
            IsHide = isHide;
            if (resourceType != null && 
                !string.IsNullOrEmpty(titleResourceName) && 
                !string.IsNullOrEmpty(messageResourceName))
            {
                Title = ResourceHelper.GetResourceLookup(resourceType, titleResourceName);
                Message = ResourceHelper.GetResourceLookup(resourceType, messageResourceName);
            }
        }

        public string Name { get; }
        public double Width { get; }
        public bool IsRequired { get; set; }
        public bool ProtectedColumn { get; set; }
        public bool IsSelected { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public bool IsHide { get; set; }
    }
}