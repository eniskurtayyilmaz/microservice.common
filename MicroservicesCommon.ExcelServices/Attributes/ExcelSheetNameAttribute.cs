using System;

namespace MicroservicesCommon.ExcelServices.Attributes
{
    public class ExcelSheetNameAttribute : Attribute
    {
        public ExcelSheetNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}