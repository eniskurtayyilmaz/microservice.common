using System;

namespace MicroservicesCommon.ExcelServices.Attributes
{
    public class ExcelDescriptionColumnAttribute : Attribute
    {
        public ExcelDescriptionColumnAttribute(string name, double width = 80)
        {
            Name = name;
            Width = width;
        }

        public string Name { get; }
        public double Width { get; }
    }
}