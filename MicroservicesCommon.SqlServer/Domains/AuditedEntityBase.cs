using System;

namespace MicroservicesCommon.SqlServer.Domains
{
    public abstract class AuditedEntityBase : IEntityBase
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}