namespace MicroservicesCommon.SqlServer.Common
{
    public static class Constants
    {
        public const string DatabaseConnectionStringKey = "AppDbContext";
    }
}