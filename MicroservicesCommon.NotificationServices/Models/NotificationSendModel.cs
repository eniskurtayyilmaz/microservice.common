using System.Collections.Generic;
using MicroservicesCommon.Exceptions;

namespace MicroservicesCommon.NotificationServices.Models
{
    public class NotificationSendModel
    {
        public NotificationType NotificationType { get; internal set; }
        public string TemplateName { get; internal set; }
        public string ReceiverMerchantId { get; internal set; }
        public Dictionary<string, string> Args { get; internal set; }

        public NotificationSendModel(NotificationType notificationType, string templateName, string receiverMerchantId,
            Dictionary<string, string> args)
        {
            NotificationType = notificationType;
            TemplateName = templateName ?? throw new BusinessRuleException("templateName not be null");
            ReceiverMerchantId = receiverMerchantId ?? throw new BusinessRuleException("receiverMerchantId not be null");
            Args = args ?? throw new BusinessRuleException("Args not be null");
        }
    }
}