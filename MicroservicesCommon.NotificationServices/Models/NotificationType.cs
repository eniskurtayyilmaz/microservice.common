namespace MicroservicesCommon.NotificationServices.Models
{
    public enum NotificationType
    {
        Email = 1,
        SMS = 2,
        All = 4
    }
}