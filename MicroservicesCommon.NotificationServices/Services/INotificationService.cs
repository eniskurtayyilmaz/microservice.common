using MicroservicesCommon.NotificationServices.Models;
using MicroservicesCommon.RabbitMQ.Services;
using MicroservicesCommon.Services;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace MicroservicesCommon.NotificationServices.Services
{
    public interface ISendNotificationQueueService : IRabbitMqProducerService
    {
    }

    internal class SendNotificationQueueService : RabbitMqProducerService, ISendNotificationQueueService
    {
        public SendNotificationQueueService(IJsonService jsonService, IUserResolverService userResolverService,
            ITraceIdResolverService traceIdResolverService, IConnectionFactory connectionFactory) : base(jsonService,
            userResolverService, traceIdResolverService, connectionFactory)
        {
        }

        protected override string ExchangeName => "send-notification-receiverMerchantId-exchange";
    }

    public interface INotificationService
    {
        void SendNotification(NotificationSendModel model);
    }

    public class NotificationService : INotificationService
    {
        private readonly ISendNotificationQueueService _sendNotificationQueueService;
        private readonly ISmsSendNotificationQueueService _smsSendNotificationQueueService;
        private readonly IJsonService _jsonService;
        private readonly ILogger<NotificationService> _logger;

        public NotificationService(ISendNotificationQueueService sendNotificationQueueService,
            ISmsSendNotificationQueueService smsSendNotificationQueueService, IJsonService jsonService,
            ILogger<NotificationService> logger)
        {
            _sendNotificationQueueService = sendNotificationQueueService;
            _smsSendNotificationQueueService = smsSendNotificationQueueService;
            _jsonService = jsonService;
            _logger = logger;
        }

        public void SendNotification(NotificationSendModel model)
        {
            _logger.LogInformation($"Called Send Notification {_jsonService.Serialize(model)}");
            switch (model.NotificationType)
            {
                case NotificationType.Email:
                    _sendNotificationQueueService.Publish(model);
                    break;
                case NotificationType.SMS:
                    _smsSendNotificationQueueService.Publish(model);
                    break;
                default:
                    _sendNotificationQueueService.Publish(model);
                    _smsSendNotificationQueueService.Publish(model);
                    break;
            }
        }
    }
}