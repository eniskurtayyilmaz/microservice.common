using MicroservicesCommon.RabbitMQ.Services;
using MicroservicesCommon.Services;
using RabbitMQ.Client;

namespace MicroservicesCommon.NotificationServices.Services
{
    public interface ISmsSendNotificationQueueService : IRabbitMqProducerService
    {
    }

    public class SmsSendNotificationQueueService : RabbitMqProducerService, ISmsSendNotificationQueueService
    {
        public SmsSendNotificationQueueService(IJsonService jsonService, IUserResolverService userResolverService,
            ITraceIdResolverService traceIdResolverService, IConnectionFactory connectionFactory) : base(jsonService,
            userResolverService, traceIdResolverService, connectionFactory)
        {
        }

        protected override string ExchangeName => "send-sms-notification-exchange";
    }
}