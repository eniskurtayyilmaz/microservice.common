using MicroservicesCommon.NotificationServices.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MicroservicesCommon.NotificationServices.Extensions
{
    public static class NotificationServiceExtension
    {
        public static void AddNotificationService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<ISendNotificationQueueService, SendNotificationQueueService>();
            services.AddSingleton<ISmsSendNotificationQueueService, SmsSendNotificationQueueService>();
            services.AddSingleton<INotificationService, NotificationService>();
        }

        public static void UseNotificationService(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetService<ISendNotificationQueueService>()?.Setup();
            app.ApplicationServices.GetService<ISmsSendNotificationQueueService>()?.Setup();
        }
    }
}