using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MicroservicesCommon.Services;
using StackExchange.Redis;

namespace MicroservicesCommon.CacheServices
{
    public class RedisCacheService : ICacheService
    {
        private readonly IConnectionMultiplexer _client;
        private readonly IServer _server;
        private readonly IJsonService _jsonService;
        private readonly IDatabase _database;

        public RedisCacheService(IConnectionMultiplexer client, IServer server, IJsonService jsonService)
        {
            _client = client;
            _server = server;
            _jsonService = jsonService;
            _database = _client.GetDatabase();
        }

        public void Set(string key, string value)
        {
            _database.StringSet(key, value);
        }

        public void Set<T>(string key, T value) where T : class
        {
            _database.StringSet(key, _jsonService.Serialize(value));
        }

        public void Set<T>(string key, T value, int db) where T : class
        {
            _client.GetDatabase(db).StringSet(key, _jsonService.Serialize(value));
        }

        public Task SetAsync(string key, object value)
        {
            return _database.StringSetAsync(key, _jsonService.Serialize(value));
        }

        public Task SetAsync(string key, object value, int db)
        {
            return _client.GetDatabase(db).StringSetAsync(key, _jsonService.Serialize(value));
        }

        public void Set(string key, object value, TimeSpan expiration)
        {
            _database.StringSet(key, _jsonService.Serialize(value), expiration);
        }

        public void Set(string key, object value, TimeSpan expiration, int db)
        {
            _client.GetDatabase(db).StringSet(key, _jsonService.Serialize(value), expiration);
        }

        public Task SetAsync(string key, object value, TimeSpan expiration)
        {
            return _database.StringSetAsync(key, _jsonService.Serialize(value), expiration);
        }

        public Task SetAsync(string key, object value, TimeSpan expiration, int db)
        {
            return _client.GetDatabase(db).StringSetAsync(key, _jsonService.Serialize(value), expiration);
        }

        public bool Set(string key, object value, TimeSpan expiration, When when)
        {
            return _database.StringSet(key, _jsonService.Serialize(value), expiration, when);
        }

        public bool Set(string key, object value, TimeSpan expiration, When when, int db)
        {
            return _client.GetDatabase(db).StringSet(key, _jsonService.Serialize(value), expiration, when);
        }

        public Task<bool> SetAsync(string key, object value, TimeSpan expiration, When when)
        {
            return _database.StringSetAsync(key, _jsonService.Serialize(value), expiration, when);
        }

        public Task<bool> SetAsync(string key, object value, TimeSpan expiration, When when, int db)
        {
            return _client.GetDatabase(db).StringSetAsync(key, _jsonService.Serialize(value), expiration, when);
        }

        public T Get<T>(string key) where T : class
        {
            string value = _database.StringGet(key);

            return string.IsNullOrEmpty(value) ? null : _jsonService.DeSerialize<T>(value);
        }

        public T Get<T>(string key, int db) where T : class
        {
            string value = _client.GetDatabase(db).StringGet(key);

            return string.IsNullOrEmpty(value) ? null : _jsonService.DeSerialize<T>(value);
        }

        public string Get(string key)
        {
            return _database.StringGet(key);
        }

        public string Get(string key, int db)
        {
            return _client.GetDatabase(db).StringGet(key);
        }

        public async Task<T> GetAsync<T>(string key) where T : class
        {
            string value = await _database.StringGetAsync(key);

            return string.IsNullOrEmpty(value) ? null : _jsonService.DeSerialize<T>(value);
        }

        public async Task<T> GetAsync<T>(string key, int db) where T : class
        {
            string value = await _client.GetDatabase(db).StringGetAsync(key);

            return string.IsNullOrEmpty(value) ? null : _jsonService.DeSerialize<T>(value);
        }

        public bool Remove(string key)
        {
            return _database.KeyDelete(key);
        }

        public bool Remove(string key, int db)
        {
            return _client.GetDatabase(db).KeyDelete(key);
        }

        public IEnumerable<string> GetKeys(string pattern)
        {
            return _server.Keys(pattern: pattern).Select(p => p.ToString());
        }

        public IEnumerable<string> GetKeys(string pattern, int db)
        {
            return _server.Keys(db, pattern).Select(p => p.ToString());
        }
    }
}