using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace MicroservicesCommon.CacheServices
{
    public interface ICacheService
    {
        void Set(string key, string value);
        void Set<T>(string key, T value) where T : class;
        void Set<T>(string key, T value, int db) where T : class;
        Task SetAsync(string key, object value);
        Task SetAsync(string key, object value, int db);
        void Set(string key, object value, TimeSpan expiration);
        void Set(string key, object value, TimeSpan expiration, int db);
        Task SetAsync(string key, object value, TimeSpan expiration);
        Task SetAsync(string key, object value, TimeSpan expiration, int db);
        bool Set(string key, object value, TimeSpan expiration, When when);
        bool Set(string key, object value, TimeSpan expiration, When when, int db);
        Task<bool> SetAsync(string key, object value, TimeSpan expiration, When when);
        Task<bool> SetAsync(string key, object value, TimeSpan expiration, When when, int db);
        T Get<T>(string key) where T : class;
        T Get<T>(string key, int db) where T : class;
        string Get(string key);
        string Get(string key, int db);
        Task<T> GetAsync<T>(string key) where T : class;
        Task<T> GetAsync<T>(string key, int db) where T : class;
        bool Remove(string key);
        bool Remove(string key, int db);
        IEnumerable<string> GetKeys(string pattern);
        IEnumerable<string> GetKeys(string pattern, int db);
    }
}