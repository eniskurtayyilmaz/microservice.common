using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace MicroservicesCommon.CacheServices
{
    public static class DependencyInjection
    {
        public static void AddRedisCacheService(this IServiceCollection services, IConfiguration configuration)
        {
            var client = ConnectionMultiplexer.Connect(new ConfigurationOptions
            {
                EndPoints =
                {
                    {
                        configuration["Redis:Host"]
                    }
                },
                AbortOnConnectFail = true,
                Password = configuration["Redis:Password"] ?? string.Empty,
                Ssl = bool.Parse(configuration["Redis:UseSsl"])
            });
            services.AddSingleton<IConnectionMultiplexer>(client);
            services.AddSingleton<IServer>(client.GetServer(configuration["Redis:Host"]));
            services.AddSingleton<ICacheService, RedisCacheService>();
            services.AddHealthChecks().AddRedis(configuration["Redis:Host"]);
        }
    }
}