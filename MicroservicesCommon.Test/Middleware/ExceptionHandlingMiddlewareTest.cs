using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using MicroservicesCommon.Common;
using MicroservicesCommon.Exceptions;
using MicroservicesCommon.Middleware;
using MicroservicesCommon.Models;
using MicroservicesCommon.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace MicroservicesCommon.Test.Middleware
{
    public class ExceptionHandlingMiddlewareTest
    {
        [Fact]
        public void It_Should_Move_Next_When_No_Exception_Thrown()
        {
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockRequestDelegate.Verify(rd => rd.Invoke(httpContext), Times.Once);
        }

        [Fact]
        public async void It_Should_Return_Error_Response_With_404_Status_When_Domain_Not_Found_Exception_Thrown()
        {
            const string domain = "domain";
            const string id = "id";
            const string traceId = "traceId";
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            var domainNotFoundException = new DomainNotFoundException(domain, id);
            mockRequestDelegate.Setup(rd => rd.Invoke(httpContext))
                .Throws(domainNotFoundException);

            mockHttpContextService.Setup(hs =>
                hs.GetContextItemValue(Constants.TraceIdHeaderName)).Returns(traceId);

            mockJsonService.Setup(j => j.Serialize(It.IsAny<ErrorResponse>()))
                .Returns("{}");

            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            await exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockLogger.Verify( x => x.Log(
                LogLevel.Warning,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                domainNotFoundException,
                It.IsAny<Func<It.IsAnyType, Exception, string>>()), Times.Once);

            httpContext.Response.ContentType.Should().Be(Constants.ContentTypeApplicationJson);
            httpContext.Response.StatusCode.Should().Be(404);
        }

        [Fact]
        public async void It_Should_Return_Error_Response_With_500_Status_When_Unhandled_Exception_Thrown()
        {
            const string traceId = "traceId";
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            var unhandledException = new Exception();
            mockRequestDelegate.Setup(rd => rd.Invoke(httpContext))
                .Throws(unhandledException);

            mockHttpContextService.Setup(hs =>
                hs.GetContextItemValue(Constants.TraceIdHeaderName)).Returns(traceId);
            mockJsonService.Setup(j => j.Serialize(It.IsAny<ErrorResponse>()))
                .Returns("{}");
            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            await exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockLogger.Verify( x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                unhandledException,
                It.IsAny<Func<It.IsAnyType, Exception, string>>()), Times.Once);

            httpContext.Response.ContentType.Should().Be(Constants.ContentTypeApplicationJson);
            httpContext.Response.StatusCode.Should().Be(500);
        }

        [Fact]
        public async void It_Should_Return_Error_Response_With_400_Status_When_Business_Rule_Exception_Thrown()
        {
            const string userMessage = "client mesajı";
            const string traceId = "traceId";
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            var businessRuleException = new BusinessRuleException(userMessage);
            mockRequestDelegate.Setup(rd => rd.Invoke(httpContext))
                .Throws(businessRuleException);

            mockHttpContextService.Setup(hs =>
                hs.GetContextItemValue(Constants.TraceIdHeaderName)).Returns(traceId);

            mockJsonService.Setup(j => j.Serialize(It.IsAny<ErrorResponse>()))
                .Returns("{}");

            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            await exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockLogger.Verify( x => x.Log(
                LogLevel.Warning,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                businessRuleException,
                It.IsAny<Func<It.IsAnyType, Exception, string>>()), Times.Once);

            httpContext.Response.ContentType.Should().Be(Constants.ContentTypeApplicationJson);
            httpContext.Response.StatusCode.Should().Be(400);
        }

        [Fact]
        public async void It_Should_Return_Error_Response_With_401_Status_When_Unauthroized_Exception_Thrown()
        {
            const string userMessage = "client mesajı";
            const string traceId = "traceId";
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            var unauthorizedException = new UnauthorizedException(userMessage);
            mockRequestDelegate.Setup(rd => rd.Invoke(httpContext))
                .Throws(unauthorizedException);

            mockHttpContextService.Setup(hs =>
                hs.GetContextItemValue(Constants.TraceIdHeaderName)).Returns(traceId);

            mockJsonService.Setup(j => j.Serialize(It.IsAny<ErrorResponse>()))
                .Returns("{}");

            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            await exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockLogger.Verify( x => x.Log(
                LogLevel.Warning,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                unauthorizedException,
                It.IsAny<Func<It.IsAnyType, Exception, string>>()), Times.Once);

            httpContext.Response.ContentType.Should().Be(Constants.ContentTypeApplicationJson);
            httpContext.Response.StatusCode.Should().Be(401);
        }

        [Fact]
        public async Task It_Should_Return_Error_Messages_When_Given_Message()
        {
            const string userMessage = "client mesajı";
            const string traceId = "traceId";
            var errorResponse = new ErrorResponse()
            {
                Status = 500,
                TraceId= traceId
            };

            errorResponse.Errors.Add(typeof(Exception).ToString(), new string[] { userMessage });
            
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockJsonService = new Mock<IJsonService>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var mockLogger = new Mock<ILogger<ExceptionHandlingMiddleware>>();
            var httpContext = new DefaultHttpContext();
            httpContext.Response.Body = new MemoryStream();
            var unhandledException = new Exception();
            mockRequestDelegate.Setup(rd => rd.Invoke(httpContext))
                .Throws(unhandledException);

            mockHttpContextService.Setup(hs =>
                hs.GetContextItemValue(Constants.TraceIdHeaderName)).Returns(traceId);

            mockJsonService.Setup(j => j.Serialize(It.IsAny<ErrorResponse>()))
                .Returns(JsonConvert.SerializeObject(errorResponse));

            var exceptionHandlingMiddleware = new ExceptionHandlingMiddleware(mockRequestDelegate.Object
                , mockJsonService.Object, mockHttpContextService.Object, mockLogger.Object);
            await exceptionHandlingMiddleware.InvokeAsync(httpContext);
            mockLogger.Verify( x => x.Log(
                LogLevel.Error,
                It.IsAny<EventId>(),
                It.IsAny<It.IsAnyType>(),
                unhandledException,
                It.IsAny<Func<It.IsAnyType, Exception, string>>()), Times.Once);

            httpContext.Response.Body.Seek(0, SeekOrigin.Begin);
            var body = new StreamReader(httpContext.Response.Body).ReadToEnd();


            var responseBody = JsonConvert.DeserializeObject<ErrorResponse>(body);

            httpContext.Response.StatusCode.Should().Be(500);
            httpContext.Response.ContentType.Should().Be(Constants.ContentTypeApplicationJson);
            Assert.Equal(userMessage, responseBody.Errors[typeof(Exception).ToString()][0]);
            Assert.Equal(errorResponse.Errors.Keys.First(), responseBody.Errors.Keys.First());
        }
    }
}