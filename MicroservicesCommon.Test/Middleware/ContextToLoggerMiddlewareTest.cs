using System.Collections.Generic;
using FluentAssertions;
using MicroservicesCommon.Common;
using MicroservicesCommon.Middleware;
using MicroservicesCommon.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test.Middleware
{
    public class ContextToLoggerMiddlewareTest
    {
        [Fact]
        public async void It_Should_Set_TraceId_And_UserId_To_Logger_Scope()
        {
            var mockRequestDelegate = new Mock<RequestDelegate>();
            var mockLogger = new Mock<ILogger<ContextToLoggerMiddleware>>();
            var mockHttpContextService = new Mock<IHttpContextService>();
            var httpContext = new DefaultHttpContext();
            const string traceId = "traceId";
            const string userId = "userId";

            var contextToLoggerMiddleware = new ContextToLoggerMiddleware(mockRequestDelegate.Object,
                mockHttpContextService.Object, mockLogger.Object);
            mockHttpContextService.Setup(h => h.GetContextItemValue(Constants.TraceIdHeaderName))
                .Returns(traceId);

            mockHttpContextService.Setup(h => h.GetContextItemValue(Constants.UserIdHeaderName))
                .Returns(userId);

            mockLogger.Setup(l => l.BeginScope(It.IsAny<List<KeyValuePair<string, object>>>()))
                .Callback<List<KeyValuePair<string, object>>>(e =>
                    {
                        e[0].Key.Should().Be(Constants.TraceIdHeaderName);
                        e[0].Value.Should().Be(traceId);
                        e[1].Key.Should().Be(Constants.UserIdHeaderName);
                        e[1].Value.Should().Be(userId);
                    }
                );

            await contextToLoggerMiddleware.InvokeAsync(httpContext);
            mockRequestDelegate.Verify(r => r.Invoke(httpContext), Times.Once);
        }
    }
}