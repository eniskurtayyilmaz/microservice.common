using FluentAssertions;
using MicroservicesCommon.Common;
using MicroservicesCommon.Middleware;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test.Middleware
{
    public class HeaderToContextMiddlewareTest
    {
        [Fact]
        public async void It_Should_Copy_User_And_Trace_Id_Header_To_Http_Context_Item()
        {
            var mockRequestDelegate = new Mock<RequestDelegate>();
            const string traceId = "traceId";
            const string userId = "userId";
            const string beymenUserEmail = "beymenUserEmail";
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers[Constants.TraceIdHeaderName] = traceId;
            httpContext.Request.Headers[Constants.UserIdHeaderName] = userId;
            httpContext.Request.Headers[Constants.BeymenUserEmailHeaderName] = beymenUserEmail;

            var headerToContextMiddleware = new HeaderToContextMiddleware(mockRequestDelegate.Object);
            await headerToContextMiddleware.InvokeAsync(httpContext);

            httpContext.Items[Constants.TraceIdHeaderName]?.ToString().Should().Be(traceId);
            httpContext.Items[Constants.UserIdHeaderName]?.ToString().Should().Be(userId);
            httpContext.Items[Constants.BeymenUserEmailHeaderName]?.ToString().Should().Be(beymenUserEmail);
            httpContext.Response.Headers[Constants.TraceIdHeaderName].ToString().Should().Be(traceId);
            mockRequestDelegate.Verify(rd => rd.Invoke(httpContext), Times.Once);
        }
    }
}