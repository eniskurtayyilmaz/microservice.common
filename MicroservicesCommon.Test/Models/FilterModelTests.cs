using DocumentFormat.OpenXml.Spreadsheet;
using FluentAssertions;
using MicroservicesCommon.Models;
using Xunit;

namespace MicroservicesCommon.Test.Models
{
    public class FilterModel : PageFilterRequest
    {
        
    }

    public class FilterModelTests
    {
        [Fact]
        public void PageFilter_Is_Valid()
        {
            var filterModel = new FilterModel();

            filterModel.Page.Should().Be(0);
            filterModel.Size.Should().Be(10);
        }
    }
}