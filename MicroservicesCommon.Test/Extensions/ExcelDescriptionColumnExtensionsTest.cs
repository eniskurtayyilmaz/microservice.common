using System;
using FluentAssertions;
using MicroservicesCommon.ExcelServices.Attributes;
using MicroservicesCommon.ExcelServices.Extensions;
using MicroservicesCommon.Test.Services.Excel;
using Xunit;

namespace MicroservicesCommon.Test.Extensions
{
    public class ExcelDescriptionColumnExtensionsTest
    {
        [Fact]
        public void IsHaveDescription_WhenTypeIsIExcelSheet_ReturnTrue()
        {
            var model = typeof(TestExcelData);

            var result = model.IsHaveDescription();

            result.Should().BeTrue();
        }

        [Fact]
        public void IsHaveDescription_WhenTypeIsIExcelSheet_ReturnFalse()
        {
            var model = typeof(TestExcelNotCorrectData);

            var result = model.IsHaveDescription();

            result.Should().BeFalse();
        }

        [Fact]
        public void GetDescription_WhenTypeIsIExcelSheet_ReturnDescription()
        {
            var model = typeof(TestExcelData);
            var expected = new ExcelDescriptionColumnAttribute("Açıklama:\nTest açıklama.");

            var result = model.GetDescription();

            result.Name.Should().Be(expected.Name);
            result.Width.Should().Be(expected.Width);
        }

        [Fact]
        public void GetDescription_WhenTypeIsNotHaveIExcelSheet_ThrowException()
        {
            var model = typeof(TestExcelNotCorrectData);

            var result = Record.Exception(() => model.GetDescription());

            result.Should().BeOfType<ArgumentException>();
        }
    }
}