﻿using FluentAssertions;
using MicroservicesCommon.ExcelServices.Extensions;
using MicroservicesCommon.Test.Services.Excel;
using Xunit;

namespace MicroservicesCommon.Test.Extensions
{
    public class ExcelConvertResponseExtensionsTest
    {
        [Fact]
        public void IsValid_WhenTitleCountNotEqual_ReturnTitleErrorMessage()
        {
            var title = new[] { "A", "B" };
            var result = title.IsValid<TestExcelData>();
            result.isValid.Should().BeFalse();
            result.errorMessage.Should().Be("Exceldeki kolon başlıkları ve yerleri değiştirilemez.");
        }

        [Fact]
        public void IsValid_WhenTitleNotEqual_ReturnNotFoundErrorMessage()
        {
            var title = new[] { "A", "B", "C", "D", "E", "F", "G" };
            var result = title.IsValid<TestExcelData>();
            result.isValid.Should().BeFalse();
            result.errorMessage.Should().Be("Dosya içerisinde data bulunamadı.");
        }

        [Fact]
        public void IsValid_WhenTitleEqual_ReturnIsValidTrue()
        {
            var title = new[] { "Id", "Barkod", "Para", "BoolTest", "DPara", "Sayı", "LSayı" };
            var result = title.IsValid<TestExcelData>();
            result.isValid.Should().BeTrue();
            result.errorMessage.Should().BeNullOrEmpty();
        }
    }
}
