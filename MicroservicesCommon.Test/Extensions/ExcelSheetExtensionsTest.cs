using System;
using FluentAssertions;
using MicroservicesCommon.ExcelServices.Extensions;
using MicroservicesCommon.Test.Services.Excel;
using Xunit;

namespace MicroservicesCommon.Test.Extensions
{
    public class ExcelSheetExtensionsTest
    {
        [Fact]
        public void IsHaveSheetName_WhenTypeIsIExcelSheet_ReturnTrue()
        {
            var model = typeof(TestExcelData);

            var result = model.IsHaveSheetName();

            result.Should().BeTrue();
        }
        
        [Fact]
        public void IsHaveSheetName_WhenTypeIsIExcelSheet_ReturnFalse()
        {
            var model = typeof(TestExcelNotCorrectData);

            var result = model.IsHaveSheetName();

            result.Should().BeFalse();
        }
        
        [Fact]
        public void GetSheetName_WhenTypeIsIExcelSheet_ReturnName()
        {
            var model = typeof(TestExcelData);

            var result = model.GetSheetName();

            result.Should().Be("Test");
        }
        
        [Fact]
        public void GetSheetName_WhenTypeIsNotHaveIExcelSheet_ThrowException()
        {
            var model = typeof(TestExcelNotCorrectData);

            var result = Record.Exception(() => model.GetSheetName());

            result.Should().BeOfType<ArgumentException>();
        }


        [Fact]
        public void GetSheetName_WhenDataIsIExcelSheet_ReturnName()
        {
            var model = new TestExcelData();

            var result = model.GetSheetName();

            result.Should().Be("Test");
        }

        [Fact]
        public void GetSheetName_WhenDataIsNotHaveIExcelSheet_ThrowException()
        {
            var model = new TestExcelNotCorrectData();

            var result = Record.Exception(() => model.GetSheetName());

            result.Should().BeOfType<ArgumentException>();
        }
    }
}