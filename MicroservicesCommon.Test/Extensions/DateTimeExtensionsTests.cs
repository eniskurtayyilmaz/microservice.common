﻿using FluentAssertions;
using MicroservicesCommon.Extensions;
using System;
using Xunit;

namespace MicroservicesCommon.Test.Extensions
{
    public class DateTimeExtensionsTests
    {
        [Theory]
        [InlineData(2021, 12, 13, 7, 1)]
        [InlineData(2021, 12, 14, 7, 1)]
        [InlineData(2021, 12, 15, 7, 1)]
        [InlineData(2021, 12, 16, 7, 1)]
        [InlineData(2021, 12, 17, 7, 1)]
        [InlineData(2021, 12, 18, 7, 2)]
        [InlineData(2021, 12, 19, 7, 2)]
        [InlineData(2021, 12, 13, 14, 2)]
        [InlineData(2021, 12, 14, 14, 2)]
        [InlineData(2021, 12, 15, 14, 2)]
        [InlineData(2021, 12, 16, 14, 2)]
        [InlineData(2021, 12, 17, 14, 3)]
        [InlineData(2021, 12, 18, 14, 3)]
        [InlineData(2021, 12, 19, 14, 3)]
        [InlineData(2021, 12, 13, 1, 0)]
        [InlineData(2021, 12, 14, 1, 0)]
        [InlineData(2021, 12, 15, 1, 0)]
        [InlineData(2021, 12, 16, 1, 0)]
        [InlineData(2021, 12, 17, 1, 0)]
        [InlineData(2021, 12, 18, 1, 1)]
        [InlineData(2021, 12, 19, 1, 1)]
        public void It_Should_Return_Sunday_Count(int year, int month, int day, int deliveryDate, int sundayCount)
        {
            DateTime orderDate = new DateTime(year, month, day);

            var result = orderDate.GetHolidayDays(deliveryDate, null);

            result.Should().Be(sundayCount);
        }

        [Theory]
        [InlineData(2021, 12, 15, 11, 00, 00, 2, 17, null)]
        [InlineData(2021, 12, 11, 11, 00, 00, 3, 15, null)]
        [InlineData(2021, 12, 15, 11, 00, 00, 0, 15, "17:00")]
        [InlineData(2021, 12, 15, 17, 05, 00, 0, 16, "17:00")]
        [InlineData(2021, 12, 11, 17, 05, 00, 0, 13, "17:00")]
        [InlineData(2021, 12, 12, 11, 00, 00, 0, 13, "17:00")]
        [InlineData(2022, 05, 07, 11, 00, 00, 2, 10, null)]
        [InlineData(2022, 05, 08, 11, 00, 00, 2, 11, null)]
        [InlineData(2022, 05, 06, 11, 00, 00, 2, 09, null)]
        [InlineData(2021, 12, 13, 11, 00, 00, 1, 14, null)]
        [InlineData(2021, 12, 14, 11, 00, 00, 1, 15, null)]
        [InlineData(2021, 12, 15, 11, 00, 00, 1, 16, null)]
        [InlineData(2021, 12, 16, 11, 00, 00, 1, 17, null)]
        [InlineData(2021, 12, 17, 11, 00, 00, 1, 18, null)]
        [InlineData(2021, 12, 18, 11, 00, 00, 1, 20, null)]
        [InlineData(2021, 12, 19, 11, 00, 00, 1, 21, null)]
        [InlineData(2021, 12, 13, 11, 00, 00, 7, 21, null)]
        [InlineData(2021, 12, 14, 11, 00, 00, 7, 22, null)]
        [InlineData(2021, 12, 15, 11, 00, 00, 7, 23, null)]
        [InlineData(2021, 12, 16, 11, 00, 00, 7, 24, null)]
        [InlineData(2021, 12, 17, 11, 00, 00, 7, 25, null)]
        [InlineData(2021, 12, 18, 11, 00, 00, 7, 27, null)]
        [InlineData(2021, 12, 19, 11, 00, 00, 7, 28, null)]
        [InlineData(2021, 12, 1, 11, 00, 00, 14, 17, null)]
        [InlineData(2021, 12, 2, 11, 00, 00, 14, 18, null)]
        [InlineData(2021, 12, 3, 11, 00, 00, 14, 20, null)]
        [InlineData(2021, 12, 4, 11, 00, 00, 14, 21, null)]
        [InlineData(2021, 12, 5, 11, 00, 00, 14, 22, null)]
        [InlineData(2021, 12, 6, 11, 00, 00, 14, 22, null)]
        [InlineData(2021, 12, 7, 11, 00, 00, 14, 23, null)]
        [InlineData(2022, 05, 09, 11, 00, 00, 0, 09, "18:00")]
        [InlineData(2022, 05, 10, 11, 00, 00, 0, 10, "18:00")]
        [InlineData(2022, 05, 11, 11, 00, 00, 0, 11, "18:00")]
        [InlineData(2022, 05, 12, 11, 00, 00, 0, 12, "18:00")]
        [InlineData(2022, 05, 13, 11, 00, 00, 0, 13, "18:00")]
        [InlineData(2022, 05, 14, 11, 00, 00, 0, 14, "18:00")]
        [InlineData(2022, 05, 14, 18, 23, 00, 0, 14, "18:30")]
        [InlineData(2022, 05, 14, 18, 00, 00, 0, 16, "18:00")]
        [InlineData(2022, 05, 15, 11, 00, 00, 0, 16, "18:00")]
        [InlineData(2022, 05, 15, 17, 00, 00, 0, 16, "18:00")]
        [InlineData(2022, 05, 09, 11, 00, 00, 2, 11, null)]
        [InlineData(2022, 05, 10, 11, 00, 00, 2, 12, null)]
        [InlineData(2022, 05, 11, 11, 00, 00, 2, 13, null)]
        [InlineData(2022, 05, 12, 11, 00, 00, 2, 14, null)]
        [InlineData(2022, 05, 13, 11, 00, 00, 2, 16, null)]
        [InlineData(2022, 05, 14, 11, 00, 00, 2, 17, null)]
        [InlineData(2022, 05, 15, 11, 00, 00, 2, 18, null)]
        [InlineData(2022, 05, 14, 17, 00, 00, 2, 17, null)]
        [InlineData(2022, 05, 15, 17, 00, 00, 2, 18, null)]
        //
        // Teslim suresi = 2
        // Siparis Tarihi = 15.12.2021 (carsamba) saat 11.00
        // Kargo sayaci = siparis dustugu saatten itibaren 2 gun geri saymaya baslar 
        // Kargo sayaci bitis tarihi = 17.12.2021 11.00
        // Teslim suresi = 2
        // Siparis Tarihi = 11.12.2021 (cumartesi) saat 11.00
        // Kargo sayaci = siparis dustugu saatten itibaren 3 gun geri saymaya baslar (pazar dahil edilir)
        // Kargo sayaci bitis tarihi = 14.12.2021 11.00
        public void It_Should_Return_DeliveryDate_When_DeliveryTime_GreaterThanOrEqual_1_shippingLastDeliveryTime_Empty(
            int year, int month, int day, int hour, int minute, int second,
            int deliveryDate, int expectedDay, string lastDeliveryHour)
        {
            DateTime orderDate = new DateTime(year, month, day, hour, minute, second);
            TimeSpan? lastDeliveryTime = null;
            if (!string.IsNullOrEmpty(lastDeliveryHour))
            {
                lastDeliveryTime = TimeSpan.Parse(lastDeliveryHour);
            }

            var result = orderDate.GetMaxPermittedShippingTime(deliveryDate, lastDeliveryTime);

            result.Value.Year.Should().Be(orderDate.Year);
            result.Value.Month.Should().Be(orderDate.Month);
            result.Value.Day.Should().Be(expectedDay);

            if (!string.IsNullOrEmpty(lastDeliveryHour))
            {
                result.Value.Hour.Should().Be(lastDeliveryTime.Value.Hours);
                result.Value.Minute.Should().Be(lastDeliveryTime.Value.Minutes);
            }
        }

        [Theory]
        [InlineData(2022, 01, 01, 06)]
        [InlineData(2022, 01, 02, 06)]
        [InlineData(2022, 01, 03, 06)]
        [InlineData(2022, 01, 04, 06)]
        [InlineData(2022, 01, 05, 06)]
        [InlineData(2022, 01, 06, 06)]
        [InlineData(2022, 01, 07, 13)]
        [InlineData(2022, 01, 08, 13)]
        [InlineData(2022, 01, 09, 13)]
        [InlineData(2022, 01, 10, 13)]
        [InlineData(2022, 01, 11, 13)]
        [InlineData(2022, 01, 12, 13)]
        [InlineData(2022, 01, 13, 13)]
        public void It_Should_Return_First_Thursday(int year, int month, int day, int expectedDay)
        {
            var date = new DateTime(year, month, day);
            var firstThursday = date.ToFirstExpectedDate(DayOfWeek.Thursday);
            firstThursday.Day.Should().Be(expectedDay);
        }

        [Theory]
        [InlineData(2022, 05, 07, 12, false)]
        [InlineData(2022, 05, 07, 17, true)]
        [InlineData(2022, 05, 08, 12, true)]
        [InlineData(2022, 05, 08, 17, true)]
        [InlineData(2022, 05, 09, 00, false)]
        public void It_Should_Return_True_When_Weekend(int year, int month, int day, int hour, bool expectedResult)
        {
            DateTime orderDate = new DateTime(year, month, day, hour, 00, 00);
            bool result = DateTimeExtensions.IsItWorkingHours(orderDate, null);
            result.Should().Be(expectedResult);
        }

        [Theory]
        [InlineData(2022, 05, 07, 12, 2, DayOfWeek.Monday)]
        [InlineData(2022, 05, 07, 17, 2, DayOfWeek.Monday)]
        [InlineData(2022, 05, 08, 12, 1, DayOfWeek.Monday)]
        [InlineData(2022, 05, 08, 17, 1, DayOfWeek.Monday)]
        public void NumberOfDayBetweenDayOfWeek_Should_Return_Day_Count_Until_Day_Of_Week(int year, int month, int day,
            int hour, int expectedDay, DayOfWeek dayOfWeek)
        {
            DateTime orderDate = new DateTime(year, month, day, hour, 00, 00);
            var result = orderDate.NumberOfDayBetweenDayOfWeek(dayOfWeek);
            result.Should().Be(expectedDay);
        }

        [Theory]
        [InlineData("2022-06-07 10:10:57", "2022-06-07 13:10:57")]
        [InlineData("2022-06-07 00:10:57", "2022-06-07 03:10:57")]
        [InlineData("2022-06-07 23:10:57", "2022-06-08 02:10:57")]
        public void ToTimeZoneDateTime_WhenStringToDate_ReturnNewDateTime(string dateTime, string expected)
        {
            var date = Convert.ToDateTime(dateTime);
            var exp = Convert.ToDateTime(expected);
            date.ToTimeZoneDateTime().Should().Be(exp);
        }
        
        [Fact]
        public void ToTimeZoneDateTime_WhenDateKindUtc_ReturnNewDateTime()
        {
            var dateTime = new DateTime(2022, 06, 07, 10, 00, 00, DateTimeKind.Utc);
            var expected = new DateTime(2022, 06, 07, 13, 00, 00);
            var date = Convert.ToDateTime(dateTime);
            var exp = Convert.ToDateTime(expected);
            date.ToTimeZoneDateTime().Should().Be(exp);
        }
                
        [Fact]
        public void ToTimeZoneDateTime_WhenDateKindLocalButIsForce_ReturnNewDateTime()
        {
            var dateTime = new DateTime(2022, 06, 07, 10, 00, 00, DateTimeKind.Local);
            var expected = new DateTime(2022, 06, 07, 13, 00, 00);
            var date = Convert.ToDateTime(dateTime);
            var exp = Convert.ToDateTime(expected);
            date.ToTimeZoneDateTime(true).Should().Be(exp);
        }
    }
}