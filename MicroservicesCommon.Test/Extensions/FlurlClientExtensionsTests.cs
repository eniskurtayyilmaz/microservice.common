using System.Collections.Generic;
using FluentAssertions;
using Flurl.Http;
using MicroservicesCommon.HttpServices;
using Xunit;

namespace MicroservicesCommon.Test.Extensions
{
    public class FlurlClientExtensionsTests
    {
        [Fact]
        public void Can_Add_Header_When_Header_Null()
        {
            var flurlClient = new FlurlClient("http://address");

            var result = flurlClient.AddHeader(null);

            result.Should().NotBeNull();
        }

        [Fact]
        public void Can_Add_Header_When_Header_Empty()
        {
            var flurlClient = new FlurlClient("http://address");

            var result = flurlClient.AddHeader(new Dictionary<string, string>());

            result.Should().NotBeNull();
        }

        [Fact]
        public void Can_Add_Header_When_Header_Not_Empty()
        {
            var flurlClient = new FlurlClient("http://address");

            var result = flurlClient.AddHeader(new Dictionary<string, string>() {{"Key", "Value"}});

            result.Should().NotBeNull();
        }
    }
}