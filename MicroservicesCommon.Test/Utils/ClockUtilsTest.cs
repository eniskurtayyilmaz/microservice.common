using System;
using FluentAssertions;
using MicroservicesCommon.Utils;
using Xunit;

namespace MicroservicesCommon.Test.Utils
{
    public class ClockUtilsTest
    {
        [Fact]
        public void It_Should_Return_Set_Date_When_Clock_Is_Frozen()
        {
            var currentDate = new DateTime(2021, 1, 19, 18, 26, 0);
            ClockUtils.Freeze();
            ClockUtils.SetDateTime(currentDate);
            var expected = ClockUtils.Now();
            expected.Should().Be(currentDate);
        }

        [Fact]
        public void It_Should_Return_Different_Date_Instance_When_Clock_Is_Not_Frozen()
        {
            var currentDate = new DateTime(2021, 1, 19, 18, 26, 0);
            ClockUtils.Freeze(currentDate);
            ClockUtils.UnFreeze();
            var expected = ClockUtils.Now();
            expected.Should().NotBe(currentDate);
        }

        [Fact]
        public void It_Should_Return_Different_Date_Instance_When_Clock_Is_Frozen()
        {
            ClockUtils.Freeze();
            var expected = ClockUtils.Now();
            expected.Should().NotBe(DateTime.Now);
        }
    }
}