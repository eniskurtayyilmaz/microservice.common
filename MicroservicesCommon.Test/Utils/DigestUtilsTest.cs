using FluentAssertions;
using MicroservicesCommon.Utils;
using Xunit;

namespace MicroservicesCommon.Test.Utils
{
    public class DigestUtilsTest
    {
        [Theory]
        [InlineData("deneme","8f10d078b2799206cfe914b32cc6a5e9")]
        [InlineData("http://localhost.com","4792d3ad2ec12f9e510ecfaf1b242a78")]
        [InlineData("","")]
        [InlineData(null,"")]
        public void Md5_WhenHasString_ReturnMd5(string value,string expected)
        {
            DigestUtils.Md5(value).Should().Be(expected);
        }
    }
}