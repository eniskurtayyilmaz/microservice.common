using FluentAssertions;
using MicroservicesCommon.Utils;
using Xunit;

namespace MicroservicesCommon.Test.Utils
{
    public class RandomUtilsTest
    {
        [Fact]
        public void It_Should_Return_Random_Alphanumeric_Chars_With_Given_Length()
        {
            var randomString = RandomUtils.RandomAlphaNumeric(20);
            randomString.Should().HaveLength(20);
        }

        [Fact]
        public void It_Should_Return_Random_Numeric_Chars_With_Given_Length()
        {
            var randomNumericString = RandomUtils.RandomNumeric(5);
            randomNumericString.Should().HaveLength(5);
            randomNumericString.ToCharArray().Should()
                .BeSubsetOf(new char[] {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'});
        }
    }
}