using FluentAssertions;
using MicroservicesCommon.Common;
using MicroservicesCommon.HttpServices;
using MicroservicesCommon.Services;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test
{
    public class HttpClientServiceTest
    {
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly Mock<IUserResolverService> _userResolverMock;
        private readonly Mock<ITraceIdResolverService> _traceIdMock;
        private readonly HttpClientService _service;

        public HttpClientServiceTest()
        {
            _configurationMock = new Mock<IConfiguration>();
            _userResolverMock = new Mock<IUserResolverService>();
            _traceIdMock = new Mock<ITraceIdResolverService>();
            _service = new HttpClientService(_configurationMock.Object, _userResolverMock.Object, _traceIdMock.Object);
        }

        [Fact]
        public void GetClient_WhenContainsKey_ReturnFlurlClient()
        {
            _configurationMock.SetupGet(p => p["base"]).Returns("localhost");
            _userResolverMock.Setup(p => p.GetCurrentUser()).Returns("test");
            _userResolverMock.Setup(p => p.GetCurrentRole()).Returns("role");
            _userResolverMock.Setup(p => p.GetBeymenUserEmail()).Returns("beymenUserEmail");
            _userResolverMock.Setup(p => p.GetCurrentRequestProject()).Returns("test");
            _traceIdMock.Setup(p => p.GetTraceId()).Returns("trace-id");

            var result = _service.GetClient("base");
            
            result.Should().NotBeNull();
            result.Headers.FirstOrDefault(Constants.UserIdHeaderName).Should().Be("test");
            result.Headers.FirstOrDefault(Constants.TraceIdHeaderName).Should().Be("trace-id");
            result.Headers.FirstOrDefault(Constants.RoleIdHeaderName).Should().Be("role");
            result.Headers.FirstOrDefault(Constants.BeymenUserEmailHeaderName).Should().Be("beymenUserEmail");
            result.Headers.FirstOrDefault(Constants.RequestProjectHeaderName).Should().Be("test");
            result.BaseUrl.Should().Be("localhost");
        }
        
        [Fact]
        public void GetClient_WhenNotContainsKey_ReturnFlurlClient()
        {
            _configurationMock.SetupGet(p => p["base"]).Returns("localhost");
            _userResolverMock.Setup(p => p.GetCurrentUser()).Returns("test");
            _userResolverMock.Setup(p => p.GetCurrentRole()).Returns("role");
            _userResolverMock.Setup(p => p.GetBeymenUserEmail()).Returns("beymenUserEmail");
            _traceIdMock.Setup(p => p.GetTraceId()).Returns("trace-id");

            var result = _service.GetClient("base2");
            
            result.Should().NotBeNull();
            result.Headers.FirstOrDefault(Constants.UserIdHeaderName).Should().Be("test");
            result.Headers.FirstOrDefault(Constants.TraceIdHeaderName).Should().Be("trace-id");
            result.Headers.FirstOrDefault(Constants.RoleIdHeaderName).Should().Be("role");
            result.Headers.FirstOrDefault(Constants.BeymenUserEmailHeaderName).Should().Be("beymenUserEmail");
            result.Headers.FirstOrDefault(Constants.RequestProjectHeaderName).Should().BeNull();
            result.BaseUrl.Should().BeNull();
        }
        
        [Fact]
        public void GetClient_WhenBaseUrlNull_ReturnFlurlClient()
        {
            _userResolverMock.Setup(p => p.GetCurrentUser()).Returns("test");
            _userResolverMock.Setup(p => p.GetCurrentRole()).Returns("role");
            _userResolverMock.Setup(p => p.GetBeymenUserEmail()).Returns("beymenUserEmail");
            _traceIdMock.Setup(p => p.GetTraceId()).Returns("trace-id");

            var result = _service.GetClient("base");
            
            result.Should().NotBeNull();
            result.Headers.FirstOrDefault(Constants.UserIdHeaderName).Should().Be("test");
            result.Headers.FirstOrDefault(Constants.TraceIdHeaderName).Should().Be("trace-id");
            result.Headers.FirstOrDefault(Constants.RoleIdHeaderName).Should().Be("role");
            result.Headers.FirstOrDefault(Constants.BeymenUserEmailHeaderName).Should().Be("beymenUserEmail");
            result.BaseUrl.Should().BeNull();
        }
        
        [Fact]
        public void GetClient_WhenResolverDataNull_ReturnFlurlClient()
        {
            var result = _service.GetClient("base");
            
            result.Should().NotBeNull();
            result.Headers.FirstOrDefault(Constants.UserIdHeaderName).Should().BeNull();
            result.Headers.FirstOrDefault(Constants.TraceIdHeaderName).Should().BeNull();
            result.Headers.FirstOrDefault(Constants.RoleIdHeaderName).Should().BeNull();
            result.Headers.FirstOrDefault(Constants.BeymenUserEmailHeaderName).Should().BeNull();
            result.BaseUrl.Should().BeNull();
        }
    }
}