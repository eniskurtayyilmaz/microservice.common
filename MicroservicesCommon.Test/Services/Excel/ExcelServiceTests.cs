using AutoFixture;
using ClosedXML.Excel;
using FluentAssertions;
using MicroservicesCommon.ExcelServices;
using MicroservicesCommon.ExcelServices.Attributes;
using MicroservicesCommon.ExcelServices.Extensions;
using MicroservicesCommon.ExcelServices.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test.Services.Excel
{
    [ExcelSheetName("Test")]
    [ExcelDescriptionColumn("Açıklama:\nTest açıklama.")]
    public class TestExcelData : IExcelSheet
    {
        [ExcelColumn("Id",isHide:true)] public string Id { get; set; }
        [ExcelColumn("Barkod")] public string Barcode { get; set; }
        [ExcelColumn("Para")] public decimal Money { get; set; }
        [ExcelColumn("BoolTest")] public bool IsTest { get; set; }
        [ExcelColumn("DPara")] public double DoubleMoney { get; set; }
        [ExcelColumn("Sayı")] public int Count { get; set; }
        [ExcelColumn("LSayı")] public long LongCount { get; set; }

    }

    [ExcelSheetName("Test")]
    public class TestTemplateExcelData : IExcelSheet
    {
        [ExcelColumn("Barkod")] public string Barcode { get; set; }
        [ExcelColumn("Para")] public decimal Money { get; set; }
        [ExcelColumn("Kdv Oranı")] public string IsTest { get; set; }
        [ExcelColumn("Cinsiyet")] public string Gender { get; set; }
        [ExcelColumn("Satıcı Renk", isRequired: false, ProtectedColumn = true)] public string Color { get; set; }
        [ExcelColumn("Kategori", ProtectedColumn = true)] public string Category { get; set; }
        [ExcelProductAtt] public List<ExcelAttributeParse> Attributes { get; set; }
    }
    [ExcelSheetName("Destek")]
    public class TestSupportExcelData : IExcelSheet
    {
        [ExcelColumn("Barkod")] public string Barcode { get; set; }
    }


    public class TestExcelNotCorrectData : IExcelSheet
    {
        public string Barcode { get; set; }
    }

    public class ExcelServiceTests
    {
        private readonly IExcelService _excelService;

        public ExcelServiceTests()
        {
            var configuration = new Mock<IConfiguration>();
            configuration.Setup(c => c["ZoneId"]).Returns("Turkey Standard Time\\Dynamic DST");
            _excelService = new ExcelService(configuration.Object);
        }

        [Fact]
        public void GenerateExcel_WhenDataCorrect_GenerateData()
        {
            var fixture = new Fixture();
            var test = fixture.Build<TestExcelData>()
                .With(x => x.Barcode, "Barcode1")
                .CreateMany(5);

            var ms = new MemoryStream();
            _excelService.GenerateExcel(test, ms);

            ms.Length.Should().BeGreaterThan(0);
            ms.Capacity.Should().BeGreaterThan(0);
            ms.Position.Should().BeGreaterThan(0);
            ms.CanRead.Should().BeTrue();
        }

        [Fact]
        public void GenerateExcel_WhenSheetNameNotExists_ThrowException()
        {
            var test = new List<TestExcelNotCorrectData>();

            var ms = new MemoryStream();

            var record = Record.Exception(() => _excelService.GenerateExcel(test, ms));

            record.Should().NotBeNull();
        }

        [Fact]
        public void GenerateExcel_WhenColumnNotExists_ThrowException()
        {
            var test = new List<TestExcelNotCorrectData>();

            var ms = new MemoryStream();

            var record = Record.Exception(() => _excelService.GenerateExcel(test, ms));

            record.Should().NotBeNull();
        }

        [Fact]
        public void GetExcelConvertResponse_WhenStreamNotNull_ReturnExcelData()
        {
            var fixture = new Fixture();
            var test = fixture.Build<TestExcelData>().CreateMany(100);

            var ms = new MemoryStream();
            _excelService.GenerateExcel(test, ms);
            File.WriteAllBytes("test.xlsx", ms.ToArray());
            var expected = _excelService.GetExcelConvertResponse<TestExcelData>(ms);

            expected.Titles.Should().Contain("Barkod");
            expected.Items.Should().BeEquivalentTo(test);
        }

        [Fact]
        public void GetExcelConvertResponse_WhenStreamIsNull_ThrowException()
        {
            var expected = _excelService.GetExcelConvertResponse<TestExcelData>(null);

            expected.Titles.Should().BeNull();
            expected.Items.Should().BeEmpty();
        }


        [Fact]
        public void GenerateTemplateExcel_It_Should_Work_Correctly()
        {
            var test = new TestTemplateExcelData();
            test.Category = "12345";
            test.Color = "12345";
            var fixture = new Fixture();

            var attributes = fixture.Build<ExcelAttribute>()
                .With(a => a.IsFreeText, false).CreateMany<ExcelAttribute>(5).ToList();
            var ms = new MemoryStream();

            _excelService.GenerateTemplateExcel(test, attributes, ms, null);

            var excelHeaders = typeof(TestTemplateExcelData).GetProperties().Where(p => p.CustomAttributes.Any(x => x.AttributeType == typeof(ExcelColumnAttribute)))
                .Select(a => new
                {
                    Name = ((ExcelColumnAttribute)a.GetCustomAttributes(typeof(object), true)[0]).Name?.ToString()
                }).Select(a => a.Name).ToArray();

            var attributesHeader = attributes.Select(a => a.Value).ToArray();
            var expectedHeader = excelHeaders.Union(attributesHeader);

            using (var workbook = new XLWorkbook(ms))
            {
                workbook.TryGetWorksheet("Test", out IXLWorksheet worksheet);
                var columns = worksheet.ColumnsUsed();
                var headers = worksheet.Row(1).CellsUsed().Select(x => x.Value).ToList();
                headers.Should().NotBeEmpty();
                headers.Should().HaveCount(expectedHeader.Count());
                headers.Should().Contain(expectedHeader);
            }

        }

        [Fact]
        public void GenerateTemplateConvertResult_When_Excel_Correct_Return_Values()
        {
            var fixture = new Fixture();
            const int attributeCount = 5;
            const int propertyCount = 6;
            var attributes = fixture.CreateMany<ExcelAttribute>(attributeCount).ToList();
            var data = fixture.Build<TestTemplateExcelData>().Without(a => a.Attributes).CreateMany(4);
            var test = new TestTemplateExcelData();
            test.Category = "12345";

            var ms = new MemoryStream();

            _excelService.GenerateTemplateExcel(test, attributes, ms, null);

            var excelConvertResponseData = new ExcelConvertResponse<TestTemplateExcelData>();

            using (var workbook = new XLWorkbook(ms))
            {
                var worksheet = workbook.Worksheet(typeof(TestTemplateExcelData).GetSheetName());

                worksheet.Cell(2, 1).InsertData(data);

                workbook.SaveAs(ms);

                var result = _excelService.GenerateTemplateConvertResult<TestTemplateExcelData>(ms);
                result.Titles.Should().HaveCount(propertyCount + attributeCount);
                result.Items.Should().HaveCount(4);
            }
        }

        [Fact]
        public void GenerateTemplateExcel_Add_Given_Sheet_When_IXLWorksheet_Not_Null()
        {
            var fixture = new Fixture();
            var testDate = new TestTemplateExcelData();
            var supportWorkBookData = new TestSupportExcelData();
            var attributes = fixture.CreateMany<ExcelAttribute>(5).ToList();

            var ms = new MemoryStream();
            var supportWorkBookMs = new MemoryStream();

            _excelService.GenerateTemplateExcel(supportWorkBookData, attributes, supportWorkBookMs, null);

            var supportSheet = _excelService.GetXLWorksheetFromStream(supportWorkBookMs, "Destek");

            _excelService.GenerateTemplateExcel(testDate, attributes, ms, supportSheet);

            using (var workbook = new XLWorkbook(ms))
            {
                var isHaveWorkSheet = workbook.TryGetWorksheet("Destek", out IXLWorksheet worksheet);
                worksheet.Should().NotBeNull();
                isHaveWorkSheet.Should().BeTrue();
            }

        }

        [Fact]
        public void GetXLWorksheetFromStream_Return_WorkSheet_When_Exist()
        {
            var fixture = new Fixture();
            var supportWorkBookData = new TestSupportExcelData();
            var attributes = fixture.CreateMany<ExcelAttribute>(5).ToList();
            var supportWorkBookMs = new MemoryStream();

            _excelService.GenerateTemplateExcel(supportWorkBookData, attributes, supportWorkBookMs, null);

            var sheet = _excelService.GetXLWorksheetFromStream(supportWorkBookMs, "Destek");
            sheet.Should().NotBeNull();
        }

        [Fact]
        public void GetXLWorksheetFromStream_Return_Null_When_WorkSheet_Not_Exisst()
        {
            var fixture = new Fixture();
            var supportWorkBookData = new TestSupportExcelData();
            var attributes = fixture.CreateMany<ExcelAttribute>(5).ToList();

            var supportWorkBookMs = new MemoryStream();

            _excelService.GenerateTemplateExcel(supportWorkBookData, attributes, supportWorkBookMs, null);

            var sheet = _excelService.GetXLWorksheetFromStream(supportWorkBookMs, "Destek2");
            sheet.Should().BeNull();
        }

    }
}