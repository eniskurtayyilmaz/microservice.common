using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using MicroservicesCommon.Models;
using MicroservicesCommon.Services;
using MicroservicesCommon.Services.Queue;
using Microsoft.Extensions.Logging;
using Moq;
using RabbitMQ.Client;
using Xunit;

namespace MicroservicesCommon.Test.Services.Queue
{
    public class QueueConsumerServiceTest
    {
        [Fact]
        public void It_Should_Bind_To_Queue_With_Dead_Letter_Config()
        {
            var mockIJsonService = new Mock<IJsonService>();
            var mockIConnectionFactory = new Mock<IConnectionFactory>();
            var mockILogger = new Mock<ILogger<RabbitMqConsumerService<QueueTest>>>();
            var mockIConnection = new Mock<IConnection>();
            var mockIModel = new Mock<IModel>();

            mockIConnectionFactory.Setup(cf => cf.CreateConnection()).Returns(mockIConnection.Object);
            mockIConnection.Setup(c => c.CreateModel()).Returns(mockIModel.Object);

            Dictionary<string, object> arguments = new()
            {
                {"x-dead-letter-exchange", ""},
                {"x-dead-letter-routing-key", "DeadLetter"}
            };

            var fakeRabbitMqConsumerService = new FakeRabbitMqConsumerService(
                mockIJsonService.Object,
                mockIConnectionFactory.Object,
                mockILogger.Object);

            fakeRabbitMqConsumerService.StartConsume();

            mockIModel.Verify(m => m.QueueDeclare("DeadLetter", true, false, false, null));
            mockIModel.Verify(m => m.QueueDeclare("QueueName", true, false, false, arguments));
            mockIModel.Verify(m => m.QueueBind("QueueName", "ExchangeName", string.Empty, null));
        }


        [Fact]
        public async Task It_Should_Consume_When_Async_Process()
        {
            var fixture = new Fixture();
            var model = fixture.Create<QueueMessageModel<QueueTest>>();
            var mockIJsonService = new Mock<IJsonService>();
            var mockIConnectionFactory = new Mock<IConnectionFactory>();
            var mockILogger = new Mock<ILogger<RabbitMqConsumerService<QueueTest>>>();
            var mockIConnection = new Mock<IConnection>();
            var mockIModel = new Mock<IModel>();

            mockIConnectionFactory.Setup(cf => cf.CreateConnection()).Returns(mockIConnection.Object);
            mockIConnection.Setup(c => c.CreateModel()).Returns(mockIModel.Object);

            Dictionary<string, object> arguments = new()
            {
                {"x-dead-letter-exchange", ""},
                {"x-dead-letter-routing-key", "DeadLetter"}
            };

            var fakeRabbitMqConsumerService = new FakeRabbitMqConsumerService(
                mockIJsonService.Object,
                mockIConnectionFactory.Object,
                mockILogger.Object);

            fakeRabbitMqConsumerService.StartConsume();

            await fakeRabbitMqConsumerService.Consume(model);
            model.Data.Id.Should().Be("1");

            mockIModel.Verify(m => m.QueueDeclare("DeadLetter", true, false, false, null));
            mockIModel.Verify(m => m.QueueDeclare("QueueName", true, false, false, arguments));
            mockIModel.Verify(m => m.QueueBind("QueueName", "ExchangeName", string.Empty, null));
        }
    }
}