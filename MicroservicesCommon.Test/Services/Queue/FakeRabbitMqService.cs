using MicroservicesCommon.RabbitMQ.Services;
using MicroservicesCommon.Services;
using MicroservicesCommon.Services.Queue;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;

namespace MicroservicesCommon.Test.Services.Queue
{
    public class FakeRabbitMqProducerService : RabbitMqProducerService
    {
        public FakeRabbitMqProducerService(IJsonService jsonService,
            IUserResolverService userResolverService, ITraceIdResolverService traceIdResolverService,
            IConnectionFactory connectionFactory) : base(jsonService,
            userResolverService, traceIdResolverService, connectionFactory)
        {
        }

        protected override string ExchangeName => "ExchangeName";
    }
}