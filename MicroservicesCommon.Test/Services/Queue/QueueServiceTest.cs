using System.Collections.Generic;
using System.Text;
using MicroservicesCommon.Services;
using Moq;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;
using Xunit;

namespace MicroservicesCommon.Test.Services.Queue
{
    public class QueueServiceTest
    {
        [Fact]
        public void It_Should_Setup_With_Given_Parameters()
        {
            var mockIJsonService = new Mock<IJsonService>();
            var mockIUserResolverService = new Mock<IUserResolverService>();
            var mockITraceIdResolverService = new Mock<ITraceIdResolverService>();
            var mockIConnectionFactory = new Mock<IConnectionFactory>();
            var mockIConnection = new Mock<IConnection>();
            var mockIModel = new Mock<IModel>();

            mockIConnectionFactory.Setup(cf => cf.CreateConnection()).Returns(mockIConnection.Object);
            mockIConnection.Setup(c => c.CreateModel()).Returns(mockIModel.Object);
            var fakeRabbitMqService = new FakeRabbitMqProducerService(
                mockIJsonService.Object, mockIUserResolverService.Object,
                mockITraceIdResolverService.Object, mockIConnectionFactory.Object);
            fakeRabbitMqService.Setup();

            mockIModel.Verify(m => m.ExchangeDeclare("ExchangeName", "fanout", true, false, null));
        }


        [Fact]
        public void It_Should_Publish_Queue_Message_To_Exchange()
        {
            var mockIJsonService = new Mock<IJsonService>();
            var mockIUserResolverService = new Mock<IUserResolverService>();
            var mockITraceIdResolverService = new Mock<ITraceIdResolverService>();
            var mockIConnectionFactory = new Mock<IConnectionFactory>();
            var mockIConnection = new Mock<IConnection>();
            var mockIModel = new Mock<IModel>();
            var mockIBasicProperties = new Mock<IBasicProperties>();
            const string userId = "userId";
            const string traceId = "traceId";
            const string roleId = "admin";
            const string beymenUserEmail = "beymenUserEmail";
            const string exchangeName = "ExchangeName";
            var expectedBody = Encoding.UTF8.GetBytes("test");
            const string testObj = "test";

            mockIConnectionFactory.Setup(cf => cf.CreateConnection()).Returns(mockIConnection.Object);
            mockIConnection.Setup(c => c.CreateModel()).Returns(mockIModel.Object);
            mockIJsonService.Setup(js => js.SerializeAsByteArray(testObj)).Returns(expectedBody);
            mockIModel.Setup(m => m.CreateBasicProperties()).Returns(mockIBasicProperties.Object);
            mockIUserResolverService.Setup(urs => urs.GetCurrentUser()).Returns(userId);
            mockIUserResolverService.Setup(urs => urs.GetCurrentRole()).Returns(roleId);
            mockIUserResolverService.Setup(urs => urs.GetBeymenUserEmail()).Returns(beymenUserEmail);
            mockITraceIdResolverService.Setup(trs => trs.GetTraceId()).Returns(traceId);

            var fakeRabbitMqService = new FakeRabbitMqProducerService(
                mockIJsonService.Object, mockIUserResolverService.Object,
                mockITraceIdResolverService.Object, mockIConnectionFactory.Object);
            fakeRabbitMqService.Publish(testObj);

            mockIBasicProperties.VerifySet(bp => bp.DeliveryMode = 2);
            mockIBasicProperties.VerifySet(bp => bp.Headers = new Dictionary<string, object>
            {
                {Common.Constants.UserIdHeaderName, userId},
                {Common.Constants.RoleIdHeaderName, roleId},
                {Common.Constants.TraceIdHeaderName, traceId},
                {Common.Constants.BeymenUserEmailHeaderName, beymenUserEmail}
            });
            mockIModel.Verify(
                m => m.BasicPublish(exchangeName, string.Empty, false, mockIBasicProperties.Object, expectedBody));
        }

        [Fact]
        public void It_Should_Publish_Queue_Message_To_Exchange_When_Exception()
        {
            var mockIJsonService = new Mock<IJsonService>();
            var mockIUserResolverService = new Mock<IUserResolverService>();
            var mockITraceIdResolverService = new Mock<ITraceIdResolverService>();
            var mockIConnectionFactory = new Mock<IConnectionFactory>();
            var mockIConnection = new Mock<IConnection>();
            var mockIModel = new Mock<IModel>();
            var mockIBasicProperties = new Mock<IBasicProperties>();
            const string userId = "userId";
            const string traceId = "traceId";
            const string roleId = "admin";
            const string beymenUserEmail = "beymenUserEmail";
            const string exchangeName = "ExchangeName";
            var expectedBody = Encoding.UTF8.GetBytes("test");
            const string testObj = "test";

            mockIConnectionFactory.Setup(cf => cf.CreateConnection()).Returns(mockIConnection.Object);
            mockIConnection.Setup(c => c.CreateModel()).Returns(mockIModel.Object);
            mockIConnection.Setup(p => p.CreateModel())
           .Throws(new BrokerUnreachableException(new System.Exception("test")));
            mockIJsonService.Setup(js => js.SerializeAsByteArray(testObj)).Returns(expectedBody);
            mockIModel.Setup(m => m.CreateBasicProperties()).Returns(mockIBasicProperties.Object);
            mockIUserResolverService.Setup(urs => urs.GetCurrentUser()).Returns(userId);
            mockIUserResolverService.Setup(urs => urs.GetCurrentRole()).Returns(roleId);
            mockIUserResolverService.Setup(urs => urs.GetBeymenUserEmail()).Returns(beymenUserEmail);
            mockITraceIdResolverService.Setup(trs => trs.GetTraceId()).Returns(traceId);

            var fakeRabbitMqService = new FakeRabbitMqProducerService(
                mockIJsonService.Object, mockIUserResolverService.Object,
                mockITraceIdResolverService.Object, mockIConnectionFactory.Object);
            var record = Record.Exception(() => fakeRabbitMqService.Publish(testObj));

            mockIConnection.Verify(m => m.CreateModel(), Times.Exactly(3));
        }
    }
}