using System.Threading.Tasks;
using MicroservicesCommon.Models;
using MicroservicesCommon.Services;
using MicroservicesCommon.Services.Queue;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace MicroservicesCommon.Test.Services.Queue
{
    public class FakeRabbitMqConsumerService : RabbitMqConsumerService<QueueTest>
    {
        public FakeRabbitMqConsumerService(IJsonService jsonService, IConnectionFactory connectionFactory,
            ILogger<RabbitMqConsumerService<QueueTest>> logger) : base(jsonService, connectionFactory, logger)
        {
        }

        protected override string QueueName => "QueueName";
        protected override string ExchangeName => "ExchangeName";
        protected override string DeadLetter => "DeadLetter";

        public override async Task Consume(QueueMessageModel<QueueTest> message)
        {
            await Task.Run(async () =>
            {
                await Task.Delay(500);
                return message.Data.Id = "1";
            });
        }
    }

    public class QueueTest
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}