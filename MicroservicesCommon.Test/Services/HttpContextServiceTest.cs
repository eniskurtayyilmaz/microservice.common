using FluentAssertions;
using MicroservicesCommon.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Moq;
using System.Net.Http.Headers;
using Xunit;

namespace MicroservicesCommon.Test.Services
{
    public class HttpContextServiceTest
    {
        [Fact]
        public void It_Should_Return_Context_Item_Of_Given_Key()
        {
            const string key = "key";
            const string value = "value";
            var mockIHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext { Items = { [key] = value } };
            mockIHttpContextAccessor.Setup(ca => ca.HttpContext).Returns(context);
            var httpContextService = new HttpContextService(mockIHttpContextAccessor.Object);
            httpContextService.GetContextItemValue(key).Should().Be(value);
        }

        [Fact]
        public void It_Should_Return_Empty_Value_Of_Given_Key_When_Key_Is_Not_Present()
        {
            const string key = "key";
            var mockIHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            mockIHttpContextAccessor.Setup(ca => ca.HttpContext).Returns(context);
            var httpContextService = new HttpContextService(mockIHttpContextAccessor.Object);
            httpContextService.GetContextItemValue(key).Should().BeNull();
        }

        [Fact]
        public void It_Should_Return_Empty_Value_Of_Given_Key_When_Context_Is_Not_Present()
        {
            const string key = "key";
            var mockIHttpContextAccessor = new Mock<IHttpContextAccessor>();
            mockIHttpContextAccessor.Setup(ca => ca.HttpContext).Returns((HttpContext)null);
            var httpContextService = new HttpContextService(mockIHttpContextAccessor.Object);
            httpContextService.GetContextItemValue(key).Should().BeNull();
        }

        [Fact]
        public void It_Should_Return_Authorization_Header_Value_When_Authorization_Header_Present()
        {
            var mockIHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var fakeAuthenticationValue = AuthenticationHeaderValue.Parse("Bearer someToken");
            mockIHttpContextAccessor.Setup(a => a.HttpContext.Request.Headers[HeaderNames.Authorization]).Returns("Bearer someToken");

            var httpContextService = new HttpContextService(mockIHttpContextAccessor.Object);

            httpContextService.GetRequestHeaderValue(HeaderNames.Authorization).Should().Be("Bearer someToken");
        }

        [Fact]
        public void It_Should_Return_Empty_Value_When_Authorization_Header_Is_Not_Present()
        {
            var mockIHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            mockIHttpContextAccessor.Setup(ca => ca.HttpContext).Returns(context);
            var httpContextService = new HttpContextService(mockIHttpContextAccessor.Object);

            httpContextService.GetRequestHeaderValue(HeaderNames.Authorization).Should().BeEmpty();
        }
    }
}