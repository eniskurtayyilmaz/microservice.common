using System;
using System.IO;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using DocumentFormat.OpenXml.Office2010.Excel;
using FluentAssertions;
using MicroservicesCommon.BlobServices;
using MicroservicesCommon.BlobServices.Models;
using MicroservicesCommon.Utils;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test.Services
{
    class TestBlobService : AzureBlobStorageService
    {
        public TestBlobService(BlobServiceClient blobServiceClient) : base(blobServiceClient)
        {
        }

        protected override string ContainerName => "test";
    }

    public class AzureBlobStorageServiceTest
    {
        private readonly Mock<BlobServiceClient> _blobServiceClient;
        private readonly TestBlobService _blobService;
        private readonly Mock<BlobContainerClient> _blobContainerClient;
        private readonly Mock<BlobClient> _blobClient;


        public AzureBlobStorageServiceTest()
        {
            _blobClient = new Mock<BlobClient>();
            _blobServiceClient = new Mock<BlobServiceClient>();
            _blobContainerClient = new Mock<BlobContainerClient>();
            _blobServiceClient.Setup(p => p.GetBlobContainerClient("test")).Returns(_blobContainerClient.Object);

            _blobService = new TestBlobService(_blobServiceClient.Object);
        }

        [Fact]
        public async Task GetBlobAsync_WhenBlobIsExists_ReturnArray()
        {
            var path = "test";
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = await _blobService.GetBlobAsync(path);
            _blobClient.Verify(p => p.DownloadToAsync(It.IsAny<MemoryStream>()), Times.Once);
        }

        [Fact]
        public async Task DeleteBlobAsync_WhenIfBlobExists_ReturnTrue()
        {
            var path = "test";
            var mockResponse = new Mock<Response<bool>>();
            mockResponse.Setup(p => p.Value).Returns(true);
            _blobClient.Setup(p => p.ExistsAsync(default)).ReturnsAsync(mockResponse.Object);
            _blobClient.Setup(p => p.DeleteIfExistsAsync(default, default, default)).ReturnsAsync(mockResponse.Object);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = await _blobService.DeleteBlobAsync(path);
            result.Should().BeTrue();
        }

        [Fact]
        public async Task DeleteBlobAsync_WhenBlobIsNotExists_ReturnTrue()
        {
            var path = "test";
            var mockResponse = new Mock<Response<bool>>();
            mockResponse.Setup(p => p.Value).Returns(false);
            _blobClient.Setup(p => p.ExistsAsync(default)).ReturnsAsync(mockResponse.Object);
            _blobClient.Setup(p => p.DeleteIfExistsAsync(default, default, default)).ReturnsAsync(mockResponse.Object);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = await _blobService.DeleteBlobAsync(path);
            result.Should().BeFalse();
        }

        [Fact]
        public async Task UploadFile_WhenBlobIsExits_ReturnUrl()
        {
            var path = "test";
            var uri = new Uri("http://test.com");
            var mockResponse = new Mock<Response<BlobContentInfo>>();
            _blobClient.Setup(p => p.UploadAsync(It.IsAny<MemoryStream>(), true, default))
                .ReturnsAsync(mockResponse.Object);
            _blobClient.Setup(p => p.Uri).Returns(uri);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var ms = new MemoryStream();
            var result = await _blobService.UploadFile(path, ms);
            result.Should().Be(uri);
            ms.CanSeek.Should().BeTrue();
        }

        [Fact]
        public void GenerateSasUrl_WhenCanGenerateSasUriFalse_ThrowException()
        {
            var path = "test";
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            _blobContainerClient.Setup(p => p.CanGenerateSasUri).Returns(false);
            var result = Record.Exception(() => _blobService.GenerateSasUrl(path));
            result.Should().NotBeNull();
            result.Message.Should().Be("Url can not generate");
        }

        [Fact]
        public void GenerateSasUrl_WhenCanGenerateSasUriTrue_ReturnResponse()
        {
            ClockUtils.Freeze();
            ClockUtils.SetDateTime(DateTime.Now);
            var expire = ClockUtils.Now().AddMinutes(20);
            var path = "test";
            var expected = new Uri("http://test.com");
            var expectedModel = new BlobStorageSasUrlResponse
            {
                ExpireDate = expire.Ticks,
                Url = expected.AbsoluteUri
            };
            _blobClient.Setup(p => p.GenerateSasUri(BlobSasPermissions.Write, expire)).Returns(expected);
            _blobClient.Setup(p => p.CanGenerateSasUri).Returns(true);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = _blobService.GenerateSasUrl(path);
            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expectedModel);
        }
        
        [Fact]
        public void GenerateDownloadUri_WhenBlobNotExists_ThrowException()
        {
            var path = "test";
            var mockResponse = new Mock<Response<bool>>();
            mockResponse.Setup(p => p.Value).Returns(false);
            _blobClient.Setup(p => p.Exists(default)).Returns(mockResponse.Object);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = Record.Exception(() => _blobService.GenerateDownloadUri(path));
            result.Should().NotBeNull();
            result.Message.Should().Be("File not found.");
        }
        
        [Fact]
        public void GenerateDownloadUri_WhenCanGenerateSasUriFalse_ThrowException()
        {
            var path = "test";
            var mockResponse = new Mock<Response<bool>>();
            mockResponse.Setup(p => p.Value).Returns(true);
            _blobClient.Setup(p => p.CanGenerateSasUri).Returns(false);
            _blobClient.Setup(p => p.Exists(default)).Returns(mockResponse.Object);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = Record.Exception(() => _blobService.GenerateDownloadUri(path));
            result.Should().NotBeNull();
            result.Message.Should().Be("Url can not generate");
        }
        
        [Fact]
        public void GenerateDownloadUri_WhenCanGenerateSasUriTrue_ReturnModel()
        {
            ClockUtils.Freeze();
            ClockUtils.SetDateTime(DateTime.Now);
            var expire = ClockUtils.Now().AddMinutes(5);
            var path = "test";
            var expected = new Uri("http://test.com");
            var expectedModel = new BlobStorageSasUrlResponse
            {
                ExpireDate = expire.Ticks,
                Url = expected.AbsoluteUri
            };
            var mockResponse = new Mock<Response<bool>>();
            mockResponse.Setup(p => p.Value).Returns(true);
            _blobClient.Setup(p => p.Exists(default)).Returns(mockResponse.Object);
            _blobClient.Setup(p => p.CanGenerateSasUri).Returns(true);
            _blobClient.Setup(p => p.GenerateSasUri(BlobSasPermissions.Read, expire)).Returns(expected);
            _blobContainerClient.Setup(p => p.GetBlobClient(path)).Returns(_blobClient.Object);
            var result = _blobService.GenerateDownloadUri(path);
            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expectedModel);
        }
        
        [Fact]
        public void GenerateContainerSasUrl_WhenCanGenerateSasUriFalse_ThrowException()
        {
            var path = "test";
            _blobContainerClient.Setup(p => p.CanGenerateSasUri).Returns(false);
            var result = Record.Exception(() => _blobService.GenerateContainerSasUrl(path));
            result.Should().NotBeNull();
            result.Message.Should().Be("Url can not generate");
        }
        
        [Fact]
        public void GenerateContainerSasUrl_WhenCanGenerateSasUriTrue_ReturnModel()
        {
            ClockUtils.Freeze();
            ClockUtils.SetDateTime(DateTime.Now);
            var expire = ClockUtils.Now().AddMinutes(20);
            var endpoint = "/test";
            var generatedSasUrl = new Uri("http://test.com/container?tick=123123");
            var expectedUri= new Uri($"http://test.com/container{endpoint}?tick=123123");
            var expectedModel = new BlobStorageSasUrlResponse
            {
                ExpireDate = expire.Ticks,
                Url = expectedUri.AbsoluteUri
            };
            _blobContainerClient.Setup(p => p.CanGenerateSasUri).Returns(true);
            _blobContainerClient.Setup(p => p.GenerateSasUri(BlobContainerSasPermissions.Write, expire))
                .Returns(generatedSasUrl);
            var result =  _blobService.GenerateContainerSasUrl(endpoint);
            result.Should().NotBeNull();
            result.Should().BeEquivalentTo(expectedModel);
        }

    }
}