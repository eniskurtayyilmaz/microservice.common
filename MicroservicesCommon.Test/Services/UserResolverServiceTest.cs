using FluentAssertions;
using MicroservicesCommon.Common;
using MicroservicesCommon.Services;
using Microsoft.Net.Http.Headers;
using Moq;
using Xunit;

namespace MicroservicesCommon.Test.Services
{
    public class UserResolverServiceTest
    {
        [Fact]
        public void It_Should_Return_Current_User_From_Context_When_Request_Contains_User_Item()
        {
            const string userName = "test-user";
            var mockIHttpContextService = new Mock<IHttpContextService>();
            mockIHttpContextService
                .Setup(contextAccessor => contextAccessor.GetContextItemValue(Constants.UserIdHeaderName))
                .Returns(userName);
            var userResolverService = new UserResolverService(mockIHttpContextService.Object);
            var expected = userResolverService.GetCurrentUser();
            expected.Should().Be(userName);
        }

        [Fact]
        public void It_Should_Return_Current_User_Authorization_Header_Value_When_Request_Header_Contains_Header()
        {
            var mockIHttpContextService = new Mock<IHttpContextService>();
            mockIHttpContextService.Setup(a => a.GetRequestHeaderValue(HeaderNames.Authorization)).Returns("Bearer someToken");

            var userResolverService = new UserResolverService(mockIHttpContextService.Object);
            var expected=userResolverService.GetAuthenticationHeaderValue();
            expected.Should().Be("Bearer someToken");
        }
    }
}