using FluentAssertions;
using MicroservicesCommon.Services;
using Xunit;

namespace MicroservicesCommon.Test.Services
{
    public class JsonServiceTest
    {
        [Fact]
        public void It_Should_Apply_Camel_Case_Naming_Policy()
        {
            IJsonService jsonService = new JsonService();
            var expected = jsonService.Serialize(new JsonTest() {Id = "Id", Name = "Name"});
            expected.Should().Be("{\"id\":\"Id\",\"name\":\"Name\"}");
        }

        [Fact]
        public void It_Should_De_Serialize_To_Given_Class()
        {
            IJsonService jsonService = new JsonService();
            var expected = jsonService.DeSerialize<JsonTest>("{\"id\":\"Id\",\"name\":\"Name\"}");
            expected.Id.Should().Be("Id");
            expected.Name.Should().Be("Name");
        }
    }

    public class JsonTest
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}