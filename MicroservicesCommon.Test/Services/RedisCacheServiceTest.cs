using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using MicroservicesCommon.CacheServices;
using MicroservicesCommon.Services;
using Moq;
using Newtonsoft.Json;
using StackExchange.Redis;
using Xunit;

namespace MicroservicesCommon.Test.Services
{
    public class RedisCacheServiceTestModel
    {
        public string Name { get; set; }
    }

    public class RedisCacheServiceTest
    {
        private readonly RedisCacheService _service;
        private readonly Mock<IConnectionMultiplexer> _mockConnectionRedis;
        private readonly Mock<IJsonService> _mockJsonService;
        private readonly Mock<IServer> _mockRedisServer;
        private readonly Mock<IDatabase> _mockDatabase;
        private readonly Fixture _fixture;

        public RedisCacheServiceTest()
        {
            _fixture = new Fixture();
            _mockConnectionRedis = new Mock<IConnectionMultiplexer>();
            _mockRedisServer = new Mock<IServer>();
            _mockJsonService = new Mock<IJsonService>();
            _mockDatabase = new Mock<IDatabase>();
            _mockConnectionRedis
                .Setup(p => p.GetDatabase(-1, It.IsAny<object>()))
                .Returns(_mockDatabase.Object);
            _mockConnectionRedis
                .Setup(p => p.GetDatabase(1, It.IsAny<object>()))
                .Returns(_mockDatabase.Object);
            _service = new RedisCacheService(_mockConnectionRedis.Object, _mockRedisServer.Object,
                _mockJsonService.Object);
        }

        [Fact]
        public void Set_WhenKeyAndValue_AddToRedis()
        {
            string key = "test", value = "test";
            _service.Set(key, value);
            _mockDatabase.Verify(p => p.StringSet(key, value, default, default, default));
        }

        [Fact]
        public void Set_WhenKeyAndValueClass_AddToRedis()
        {
            var value = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            _service.Set(key, value);
            _mockDatabase.Verify(p => p.StringSet(key, expectedValue, default, default, default));
        }

        [Fact]
        public void Set_WhenKeyAndValueClassWithDb_AddToRedis()
        {
            var value = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            _service.Set(key, value, 1);
            _mockDatabase.Verify(p => p.StringSet(key, expectedValue, default, default, default));
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObject_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value);
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, default, default, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObjectWithDb_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value, 1);
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, default, default, default));
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public void Set_WhenKeyAndValueObjectAndExpirationTime_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            _service.Set(key, value, TimeSpan.Zero);
            _mockDatabase.Verify(p => p.StringSet(key, expectedValue, TimeSpan.Zero, default, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObjectAndExpirationTime_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value, TimeSpan.Zero);
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, TimeSpan.Zero, default, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObjectAndExpirationTimeWithDb_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value, TimeSpan.Zero, 1);
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, TimeSpan.Zero, default, default));
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public void Set_WhenKeyAndValueObjectAndExpirationTimeAndWhen_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            _service.Set(key, value, TimeSpan.Zero, When.NotExists);
            _mockDatabase.Verify(p => p.StringSet(key, expectedValue, TimeSpan.Zero, When.NotExists, default));
        }

        [Fact]
        public void Set_WhenKeyAndValueObjectAndExpirationTimeAndWhenWithDb_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            _service.Set(key, value, TimeSpan.Zero, When.NotExists, 1);
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
            _mockDatabase.Verify(p => p.StringSet(key, expectedValue, TimeSpan.Zero, When.NotExists, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObjectAndExpirationTimeAndWhen_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value, TimeSpan.Zero, When.NotExists);
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, TimeSpan.Zero, When.NotExists, default));
        }

        [Fact]
        public async Task SetAsync_WhenKeyAndValueObjectAndExpirationTimeAndWhenWithDb_AddToRedis()
        {
            var value = _fixture.Create<object>();
            string key = "test", expectedValue = "test";
            _mockJsonService.Setup(p => p.Serialize(value)).Returns(expectedValue);
            await _service.SetAsync(key, value, TimeSpan.Zero, When.NotExists, 1);
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
            _mockDatabase.Verify(p => p.StringSetAsync(key, expectedValue, TimeSpan.Zero, When.NotExists, default));
        }

        [Fact]
        public void Get_WhenKeyCorrect_ReturnModel()
        {
            var model = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGet(key, default)).Returns(value);
            _mockJsonService.Setup(p => p.DeSerialize<RedisCacheServiceTestModel>(value)).Returns(model);
            var result = _service.Get<RedisCacheServiceTestModel>(key);
            result.Should().Be(model);
        }

        [Fact]
        public void Get_WhenKeyCorrectWithDb_ReturnModel()
        {
            var model = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGet(key, default)).Returns(value);
            _mockJsonService.Setup(p => p.DeSerialize<RedisCacheServiceTestModel>(value)).Returns(model);
            var result = _service.Get<RedisCacheServiceTestModel>(key, 1);
            result.Should().Be(model);
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public void Get_WhenKeyCorrect_ReturnString()
        {
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGet(key, default)).Returns(value);
            var result = _service.Get(key);
            result.Should().Be(value);
        }

        [Fact]
        public void Get_WhenKeyCorrectWithDb_ReturnString()
        {
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGet(key, default)).Returns(value);
            var result = _service.Get(key, 1);
            result.Should().Be(value);
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public async Task GetAsync_WhenKeyCorrect_ReturnModel()
        {
            var model = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGetAsync(key, default)).ReturnsAsync(value);
            _mockJsonService.Setup(p => p.DeSerialize<RedisCacheServiceTestModel>(value)).Returns(model);
            var result = await _service.GetAsync<RedisCacheServiceTestModel>(key);
            result.Should().Be(model);
        }

        [Fact]
        public async Task GetAsync_WhenKeyCorrectWithDb_ReturnModel()
        {
            var model = _fixture.Create<RedisCacheServiceTestModel>();
            string key = "test", value = "test";
            _mockDatabase.Setup(p => p.StringGetAsync(key, default)).ReturnsAsync(value);
            _mockJsonService.Setup(p => p.DeSerialize<RedisCacheServiceTestModel>(value)).Returns(model);
            var result = await _service.GetAsync<RedisCacheServiceTestModel>(key, 1);
            result.Should().Be(model);
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public void Remove_WhenKeyIsExists_ReturnTrue()
        {
            string key = "test";
            _mockDatabase.Setup(p => p.KeyDelete(key, default)).Returns(true);
            var result = _service.Remove(key);
            result.Should().BeTrue();
        }

        [Fact]
        public void Remove_WhenKeyIsExistsWithDb_ReturnTrue()
        {
            string key = "test";
            _mockDatabase.Setup(p => p.KeyDelete(key, default)).Returns(true);
            var result = _service.Remove(key, 1);
            result.Should().BeTrue();
            _mockConnectionRedis.Verify(p => p.GetDatabase(1, default));
        }

        [Fact]
        public void GetKeys_WhenPatternIsExists_ReturnKeyList()
        {
            var list = _fixture.CreateMany<RedisKey>();
            string pattern = "test-*";
            _mockRedisServer
                .Setup(p => p.Keys(-1, pattern, It.IsAny<int>(), It.IsAny<long>(), It.IsAny<int>(), default))
                .Returns(list);
            var result = _service.GetKeys(pattern);
            result.Should().BeEquivalentTo(list.Select(p=>p.ToString()));
        }

        [Fact]
        public void GetKeys_WhenPatternIsExistsWithDb_ReturnKeyList()
        {
            var list = _fixture.CreateMany<RedisKey>();
            string pattern = "test-*";
            _mockRedisServer
                .Setup(p => p.Keys(1, pattern, It.IsAny<int>(), It.IsAny<long>(), It.IsAny<int>(), default))
                .Returns(list);
            var result = _service.GetKeys(pattern, 1);
            result.Should().BeEquivalentTo(list.Select(p=>p.ToString()));
        }
    }
}