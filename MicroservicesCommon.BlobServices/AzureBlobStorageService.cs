using System;
using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using MicroservicesCommon.BlobServices.Models;
using MicroservicesCommon.Utils;

namespace MicroservicesCommon.BlobServices
{
    public abstract class AzureBlobStorageService : IBlobStorageService
    {
        protected abstract string ContainerName { get; }

        private readonly BlobContainerClient _containerClient;

        protected AzureBlobStorageService(BlobServiceClient blobServiceClient)
        {
            if (string.IsNullOrEmpty(ContainerName))
            {
                throw new ArgumentException("ContainerName bulunamadı");
            }

            blobServiceClient.GetBlobContainerClient(ContainerName).CreateIfNotExists();

            _containerClient = blobServiceClient.GetBlobContainerClient(ContainerName);
        }

        public async Task<byte[]> GetBlobAsync(string path)
        {
            MemoryStream ms = new MemoryStream();
            await _containerClient.GetBlobClient(path).DownloadToAsync(ms);

            return ms.ToArray();
        }

        public async Task DownloadToAsync(string path, MemoryStream ms)
        {
            await _containerClient.GetBlobClient(path).DownloadToAsync(ms);
        }

        public async Task<bool> DeleteBlobAsync(string path)
        {
            var blobClient = _containerClient.GetBlobClient(path);
            if (await blobClient.ExistsAsync())
            {
                return await blobClient.DeleteIfExistsAsync();
            }

            return false;
        }

        public async Task<Uri> UploadFile(string path, MemoryStream ms)
        {
            ms.Seek(0, SeekOrigin.Begin);
            var blobClient = _containerClient.GetBlobClient(path);

            await blobClient.UploadAsync(ms, overwrite: true);

            return blobClient.Uri;
        }

        public BlobStorageSasUrlResponse GenerateSasUrl(string path)
        {
            var blobClient = _containerClient.GetBlobClient(path);

            if (!blobClient.CanGenerateSasUri)
            {
                throw new Exception("Url can not generate");
            }

            var expire = ClockUtils.Now().AddMinutes(20);

            Uri sasUri = blobClient.GenerateSasUri(BlobSasPermissions.Write, expire);

            return new BlobStorageSasUrlResponse
            {
                Url = sasUri.AbsoluteUri,
                ExpireDate = expire.Ticks
            };
        }

        public BlobStorageSasUrlResponse GenerateDownloadUri(string path)
        {
            var blobClient = _containerClient.GetBlobClient(path);

            if (!blobClient.Exists().Value)
            {
                throw new Exception("File not found.");
            }

            if (!blobClient.CanGenerateSasUri)
            {
                throw new Exception("Url can not generate");
            }

            var expire = ClockUtils.Now().AddMinutes(5);
            Uri sasUri = blobClient.GenerateSasUri(BlobSasPermissions.Read, expire);

            return new BlobStorageSasUrlResponse
            {
                Url = sasUri.AbsoluteUri,
                ExpireDate = expire.Ticks
            };
        }

        public BlobStorageSasUrlResponse GenerateContainerSasUrl(string endpoint)
        {
            if (!_containerClient.CanGenerateSasUri)
            {
                throw new Exception("Url can not generate");
            }

            var expire = ClockUtils.Now().AddMinutes(20);

            Uri sasUri = _containerClient.GenerateSasUri(BlobContainerSasPermissions.Write, expire);

            string absolutePath = sasUri.AbsolutePath + endpoint;
            UriBuilder sasUriBuilder =
                new UriBuilder(sasUri.Scheme, sasUri.Host, sasUri.Port, absolutePath, sasUri.Query);

            return new BlobStorageSasUrlResponse
            {
                Url = sasUriBuilder.Uri.AbsoluteUri,
                ExpireDate = expire.Ticks
            };
        }
    }
}