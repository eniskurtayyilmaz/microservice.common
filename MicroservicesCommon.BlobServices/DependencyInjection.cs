using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MicroservicesCommon.BlobServices
{
    public static class DependencyInjection
    {
        public static void AddBlobService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient(p =>
                new BlobServiceClient(configuration["AzureConfig:ConnectionString"]));
        }
    }
}