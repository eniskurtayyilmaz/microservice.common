namespace MicroservicesCommon.BlobServices.Models
{
    public class BlobStorageSasUrlResponse
    {
        public long ExpireDate { get; set; }
        public string Url { get; set; }
    }
}