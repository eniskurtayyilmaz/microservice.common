using System;
using System.IO;
using System.Threading.Tasks;
using MicroservicesCommon.BlobServices.Models;

namespace MicroservicesCommon.BlobServices
{
    public interface IBlobStorageService
    {
        public Task<byte[]> GetBlobAsync(string blobName);
        public BlobStorageSasUrlResponse GenerateSasUrl(string path);
        public Task<bool> DeleteBlobAsync(string blobName);
        public Task<Uri> UploadFile(string path, MemoryStream ms);
        public BlobStorageSasUrlResponse GenerateDownloadUri(string path);
        public BlobStorageSasUrlResponse GenerateContainerSasUrl(string endpoint);
        public Task DownloadToAsync(string path, MemoryStream ms);
    }
}