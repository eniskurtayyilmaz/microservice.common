using System;

namespace MicroservicesCommon.Exceptions
{

    public class BusinessRuleException : Exception
    {
        public string UserMessage { get; set; }
        public BusinessRuleException(string userMessage) : base($"{userMessage}")
        {
            this.UserMessage = userMessage;
        }
    }
}