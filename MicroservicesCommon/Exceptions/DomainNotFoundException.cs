using System;

namespace MicroservicesCommon.Exceptions
{
    public class DomainNotFoundException : Exception
    {
        public DomainNotFoundException(string domain, string id) : base($"{domain}-{id}")
        {
        }
    }
}