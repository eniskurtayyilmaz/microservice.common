using System;

namespace MicroservicesCommon.Exceptions
{
    public class UnauthorizedException : Exception
    {
        public string UserMessage { get; set; }
        public UnauthorizedException()
        {

        }
        public UnauthorizedException(string userMessage) : base($"{userMessage}")
        {
            this.UserMessage = userMessage;
        }
    }
}