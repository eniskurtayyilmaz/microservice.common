namespace MicroservicesCommon.Common
{
    public static class Constants
    {
        public const string TraceIdHeaderName = "x-trace-id";
        public const string UserIdHeaderName = "x-user-id";
        public const string BeymenUserEmailHeaderName = "x-beymen-user-email";
        public const string RoleIdHeaderName = "x-role-id";
        public const string AnonymousUserName = "anonymous";
        public const string AnonymousRoleId = "anonymous";
        public const string ContentTypeApplicationJson = "application/json";
        public const string RequestProjectHeaderName = "x-request-project";
    }
}