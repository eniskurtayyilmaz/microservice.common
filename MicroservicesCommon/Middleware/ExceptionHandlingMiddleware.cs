using System;
using System.Net;
using System.Threading.Tasks;
using MicroservicesCommon.Common;
using MicroservicesCommon.Exceptions;
using MicroservicesCommon.Models;
using MicroservicesCommon.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MicroservicesCommon.Middleware
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IJsonService _jsonService;
        private readonly IHttpContextService _contextService;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;

        public ExceptionHandlingMiddleware(RequestDelegate next, IJsonService jsonService,
            IHttpContextService contextService, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _jsonService = jsonService;
            _contextService = contextService;
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (UnauthorizedException unauthorizedException)
            {
                int statusCode = (int) HttpStatusCode.Unauthorized;
                _logger.LogWarning(unauthorizedException, "Unauthorized Exception Occurred");
                await CreateErrorResponse(context, unauthorizedException, statusCode,
                    _contextService.GetContextItemValue(Constants.TraceIdHeaderName));
            }
            catch (BusinessRuleException businessRuleException)
            {
                int statusCode = (int) HttpStatusCode.BadRequest;
                _logger.LogWarning(businessRuleException, "Business Rule Exception Occurred");
                await CreateErrorResponse(context, businessRuleException, statusCode,
                    _contextService.GetContextItemValue(Constants.TraceIdHeaderName));
            }
            catch (DomainNotFoundException notFoundException)
            {
                const int statusCode = (int) HttpStatusCode.NotFound;
                _logger.LogWarning(notFoundException, "Domain not found");
                await CreateErrorResponse(context, notFoundException, statusCode,
                    _contextService.GetContextItemValue(Constants.TraceIdHeaderName));
            }
            catch (Exception ex)
            {
                const int statusCode = (int) HttpStatusCode.InternalServerError;
                _logger.LogError(ex, "Unexpected Exception Occurred");
                await CreateErrorResponse(context, ex, statusCode,
                    _contextService.GetContextItemValue(Constants.TraceIdHeaderName));
            }
        }

        private async Task CreateErrorResponse(HttpContext context, Exception exception, int statusCode, string traceId)
        {
            context.Response.ContentType = Constants.ContentTypeApplicationJson;
            context.Response.StatusCode = statusCode;
            var error = new ErrorResponse
            {
                Status = statusCode,
                TraceId = traceId
            };
            error.Errors.Add(exception.GetType().ToString(), new[] {exception.Message});
            var serializedResponse = _jsonService.Serialize(error);
            await context.Response.WriteAsync(serializedResponse);
        }
    }
}