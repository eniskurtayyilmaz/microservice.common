using System.Threading.Tasks;
using MicroservicesCommon.Common;
using MicroservicesCommon.Utils;
using Microsoft.AspNetCore.Http;

namespace MicroservicesCommon.Middleware
{
    public class HeaderToContextMiddleware
    {
        private readonly RequestDelegate _next;

        public HeaderToContextMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var traceIdHeader = context.Request.Headers[Constants.TraceIdHeaderName];
            var traceId = traceIdHeader.Count > 0 ? traceIdHeader[0] : GuidUtils.New().ToString();
            var userIdHeader = context.Request.Headers[Constants.UserIdHeaderName];
            var userId = userIdHeader.Count > 0 ? userIdHeader[0] : Constants.AnonymousUserName;
            var roleIdHeader = context.Request.Headers[Constants.RoleIdHeaderName];
            var roleId = roleIdHeader.Count > 0 ? roleIdHeader[0] : Constants.AnonymousRoleId;
            var requestProjectHeader = context.Request.Headers[Constants.RequestProjectHeaderName];
            var requestProject = requestProjectHeader.Count > 0 ? requestProjectHeader[0] : string.Empty;
            var beymenUserEmailHeader = context.Request.Headers[Constants.BeymenUserEmailHeaderName];
            var beymenUserEmail = beymenUserEmailHeader.Count > 0 ? beymenUserEmailHeader[0] : Constants.BeymenUserEmailHeaderName;
            context.Items[Constants.TraceIdHeaderName] = traceId;
            context.Items[Constants.UserIdHeaderName] = userId;
            context.Items[Constants.RoleIdHeaderName] = roleId;
            context.Items[Constants.BeymenUserEmailHeaderName] = beymenUserEmail;
            if (!string.IsNullOrEmpty(requestProject))
            {
                context.Items[Constants.RequestProjectHeaderName] = requestProject;
            }
            context.Response.Headers[Constants.TraceIdHeaderName] = traceId;
            await _next(context);
        }
    }
}