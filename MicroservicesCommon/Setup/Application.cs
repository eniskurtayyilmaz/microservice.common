using System;
using Consul;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Winton.Extensions.Configuration.Consul;

namespace MicroservicesCommon.Setup
{
    public class Application
    {
        public static IHostBuilder CreateHostBuilderWithExternalConfiguration(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((context, builder) =>
                {
                    if (context.HostingEnvironment.IsDevelopment())
                    {
                        builder.AddJsonFile("appsettings.json", false, true);
                    }
                    else
                    {
                        var consulHost = context.Configuration["CONSUL_HOST"];
                        if (!string.IsNullOrEmpty(consulHost))
                        {
                            var applicationName = context.HostingEnvironment.ApplicationName;

                            void ConsulConfig(ConsulClientConfiguration configuration)
                            {
                                configuration.Address = new Uri(consulHost);
                            }

                            builder.AddConsul($"config/{applicationName}/appsettings.json",
                                source =>
                                {
                                    source.ReloadOnChange = true;
                                    source.ConsulConfigurationOptions = ConsulConfig;
                                });
                        }
                    }
                });
    }
}