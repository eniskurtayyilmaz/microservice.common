using MicroservicesCommon.Extensions;
using MicroservicesCommon.Middleware;
using MicroservicesCommon.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MicroservicesCommon.Setup
{
    public abstract class BaseStartup
    {
        protected readonly IConfiguration Configuration;

        protected BaseStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddNLog(ApplicationName);
            services.AddHttpContextAccessor();
            services.AddTransient<IHttpContextService, HttpContextService>();
            services.AddTransient<IUserResolverService, UserResolverService>();
            services.AddTransient<ITraceIdResolverService, TraceIdResolverService>();
            services.AddTransient<IJsonService, JsonService>();
            services.AddSwaggerGen();
            ConfigureServiceInternal(services);
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
            IHostApplicationLifetime applicationLifetime)
        {
            app.UseMiddleware<HeaderToContextMiddleware>();
            app.UseMiddleware<ContextToLoggerMiddleware>();
            app.UseMiddleware<ExceptionHandlingMiddleware>();
            ConfigureMiddleware(app, env);

            if (!env.IsProduction())
            {
                //app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", ApplicationName));
            }

            app.UseRouting();
            app.ConfigureHealthCheck();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            ConfigureInternal(app, env);
            
            var logger = app.ApplicationServices.GetService<ILogger<BaseStartup>>();
            applicationLifetime.ApplicationStarted.Register(() =>
            {
                logger.LogInformation($"{ApplicationName} - Application is started.");
            });
            applicationLifetime.ApplicationStopping.Register(() =>
            {
                logger.LogInformation($"{ApplicationName} - Application is shutdown.");
            });
        }

        protected abstract void ConfigureServiceInternal(IServiceCollection services);
        protected abstract void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env);
        protected abstract string ApplicationName { get; }

        protected virtual void ConfigureMiddleware(IApplicationBuilder app, IWebHostEnvironment env)
        {
        }
    }
}