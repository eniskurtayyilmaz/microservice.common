using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Config;
using NLog.Extensions.Logging;
using NLog.Layouts;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace MicroservicesCommon.Extensions
{
    public static class NLogExtension
    {
        public static void AddNLog(this IServiceCollection services, string applicationName)
        {
            //"${mdlc:x-trace-id}|${mdlc:x-user-id}|${longdate}|${uppercase:${level}}|${logger}|${message}${when:when=length('${exception}')>0:Inner=|}${exception:format=toString}"

            var layout = new JsonLayout()
            {
                Attributes =
                {
                    new JsonAttribute("x-trace-id", "${mdlc:x-trace-id}"),
                    new JsonAttribute("x-user-id", "${mdlc:x-user-id}"),
                    new JsonAttribute("log_level", "${level:upperCase=true}"),
                    new JsonAttribute("logger", "${logger}"),
                    new JsonAttribute("time", "${longdate}"),
                    new JsonAttribute("type", "${exception:format=Type}"),
                    new JsonAttribute("message", "${message}"),
                    new JsonAttribute("stackTrace", "${exception:format=ToString,StackTrace}${newline}")
                }
            };

            var loggingConfig = new LoggingConfiguration();
            var consoleTarget = new NLog.Targets.ConsoleTarget
            {
                Layout = layout
            };
            loggingConfig.AddTarget("Console", consoleTarget);
            loggingConfig.LoggingRules.Add(new LoggingRule("Microsoft", NLog.LogLevel.Off, consoleTarget));
            loggingConfig.LoggingRules.Add(new LoggingRule("Default", NLog.LogLevel.Info, consoleTarget));
            loggingConfig.LoggingRules.Add(new LoggingRule("MicroservicesCommon.*", NLog.LogLevel.Debug,
                consoleTarget));
            loggingConfig.LoggingRules.Add(new LoggingRule($"{applicationName}.*", NLog.LogLevel.Debug, consoleTarget));
            loggingConfig.LoggingRules.Add(new LoggingRule($"*", NLog.LogLevel.Warn, consoleTarget));
            LogManager.Configuration = loggingConfig;
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.AddFilter("Microsoft", LogLevel.None);
                loggingBuilder.AddFilter("Default", LogLevel.Information);
                loggingBuilder.AddFilter("MicroservicesCommon", LogLevel.Debug);
                loggingBuilder.AddFilter(applicationName, LogLevel.Debug);
                loggingBuilder.AddFilter("*", LogLevel.Warning);
                loggingBuilder.AddNLog(loggingConfig);
            });
        }
    }
}