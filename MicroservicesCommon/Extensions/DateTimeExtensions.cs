﻿using System;
using MicroservicesCommon.Utils;

namespace MicroservicesCommon.Extensions
{
    public static class DateTimeExtensions
    {
        private static TimeSpan endOfWorkingHour = new TimeSpan(17, 00, 00);

        public static int NumberOfDayBetweenDayOfWeek(this DateTime date, DayOfWeek dayOfWeek)
        {
            DateTime currentDate = date;
            while (date.DayOfWeek != dayOfWeek)
            {
                date = date.AddDays(1);
            }

            return (date - currentDate).Days;
        }

        public static bool IsItWorkingHours(DateTime dateTime, TimeSpan? lastDeliveryHour)
        {
            return (IsTheOrderAfterWork(dateTime, (lastDeliveryHour ?? endOfWorkingHour)) &&
                    dateTime.DayOfWeek == DayOfWeek.Saturday) || dateTime.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsTheOrderAfterWork(DateTime orderDate, TimeSpan timeSpan)
        {
            var orderDateTimeSpan = new TimeSpan(orderDate.Hour, orderDate.Minute, orderDate.Second);

            return orderDateTimeSpan >= timeSpan;
        }

        public static int GetHolidayDays(this DateTime orderDate, int deliveryDate, TimeSpan? lastDeliveryHour)
        {
            if (deliveryDate == 0)
            {
                if (IsItWorkingHours(orderDate, lastDeliveryHour))
                {
                    return NumberOfDayBetweenDayOfWeek(orderDate, DayOfWeek.Monday);
                }

                if (lastDeliveryHour.HasValue)
                {
                    return IsTheOrderAfterWork(orderDate, lastDeliveryHour.Value) ? 1 : 0;
                }
            }

            var res = GetNumberOfSundayDays(orderDate, orderDate.AddDays(deliveryDate));

            if (deliveryDate >= 7)
            {
                res = GetNumberOfSundayDays(orderDate, orderDate.AddDays(deliveryDate + res));
            }

            return res;
        }

        public static int GetNumberOfSundayDays(DateTime from, DateTime to)
        {
            if (to < from)
                throw new ArgumentException("To cannot be smaller than from.", nameof(to));

            if (to.Date == from.Date)
                return 0;

            int n = 0;
            DateTime nextDate = from;
            while (nextDate.Date <= to.Date)
            {
                if (nextDate.DayOfWeek == DayOfWeek.Sunday)
                    n++;
                nextDate = nextDate.AddDays(1);
            }

            return n;
        }

        public static DateTime? GetMaxPermittedShippingTime(this DateTime orderDate, int? deliveryTime,
            TimeSpan? lastDeliveryHour)
        {
            if (!deliveryTime.HasValue)
            {
                return orderDate;
            }

            var calculatedDay = orderDate.GetHolidayDays(deliveryTime.Value, lastDeliveryHour);

            var finalOrderDate = orderDate.AddDays(calculatedDay + deliveryTime.Value);

            if (lastDeliveryHour.HasValue)
            {
                return new DateTime(finalOrderDate.Year, finalOrderDate.Month, finalOrderDate.Day,
                    lastDeliveryHour.Value.Hours, lastDeliveryHour.Value.Minutes, lastDeliveryHour.Value.Seconds);
            }
            else
            {
                return finalOrderDate;
            }
        }

        public static DateTime ToFirstExpectedDate(this DateTime paymentDate, DayOfWeek dayOfWeek)
        {
            while (paymentDate.DayOfWeek != dayOfWeek)
            {
                paymentDate = paymentDate.AddDays(1);
            }

            return paymentDate;
        }

        public static DateTime ToTimeZoneDateTime(this DateTime value, bool isForce = false)
        {
            var returnedValue = value;
            if (isForce)
            {
                returnedValue = DateTime.SpecifyKind(value, DateTimeKind.Utc);
            }
            else if (returnedValue.Kind == DateTimeKind.Unspecified)
            {
                returnedValue = DateTime.SpecifyKind(value, DateTimeKind.Utc);
            }

            var timeZone = ConfigurationUtils.Configuration["ZoneId"] ?? "Europe/Istanbul";
            var systemTimeZoneById = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            return TimeZoneInfo.ConvertTime(Convert.ToDateTime(returnedValue), systemTimeZoneById);
        }
    }
}