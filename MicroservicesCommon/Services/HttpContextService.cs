using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace MicroservicesCommon.Services
{
    public interface IHttpContextService
    {
        string GetContextItemValue(string key);

        string GetRequestHeaderValue(string key);
    }

    public class HttpContextService : IHttpContextService
    {
        private readonly IHttpContextAccessor _context;

        public HttpContextService(IHttpContextAccessor context)
        {
            _context = context;
        }

        public string GetContextItemValue(string key)
        {
            return _context.HttpContext?.Items[key]?.ToString();
        }

        public string GetRequestHeaderValue(string key)
        {
            return _context.HttpContext?.Request?.Headers[key].ToString();
        }
    }
}