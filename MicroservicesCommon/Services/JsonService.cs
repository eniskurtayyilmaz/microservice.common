using System.Text;
using System.Text.Json;

namespace MicroservicesCommon.Services
{
    public interface IJsonService
    {
        string Serialize(object value);
        byte[] SerializeAsByteArray(object value);
        T DeSerialize<T>(string value);
    }

    public class JsonService : IJsonService
    {
        private readonly JsonSerializerOptions _serializeOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = false
        };

        public string Serialize(object value)
        {
            return JsonSerializer.Serialize(value, _serializeOptions);
        }

        public byte[] SerializeAsByteArray(object value)
        {
            return Encoding.UTF8.GetBytes(Serialize(value));
        }

        public T DeSerialize<T>(string value)
        {
            return JsonSerializer.Deserialize<T>(value, _serializeOptions);
        }
    }
}