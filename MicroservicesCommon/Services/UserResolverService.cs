using MicroservicesCommon.Common;
using Microsoft.Net.Http.Headers;

namespace MicroservicesCommon.Services
{
    public interface IUserResolverService
    {
        string GetCurrentUser();
        string GetCurrentRole();
        bool IsAdmin();
        string GetAuthenticationHeaderValue();
        string GetBeymenUserEmail();
        string GetCurrentRequestProject();
        bool IsSameCurrentRequestProject(string projectName);
    }

    public class UserResolverService : IUserResolverService
    {
        private readonly IHttpContextService _contextService;

        public UserResolverService(IHttpContextService contextService)
        {
            _contextService = contextService;
        }

        public string GetCurrentUser()
        {
            return _contextService.GetContextItemValue(Constants.UserIdHeaderName);
        }

        public string GetCurrentRole()
        {
            return _contextService.GetContextItemValue(Constants.RoleIdHeaderName);
        }

        public bool IsAdmin()
        {
            return GetCurrentRole() != Constants.AnonymousRoleId;
        }

        public string GetAuthenticationHeaderValue()
        {
            return _contextService.GetRequestHeaderValue(HeaderNames.Authorization);
        }

        public string GetBeymenUserEmail()
        {
            return _contextService.GetRequestHeaderValue(Constants.BeymenUserEmailHeaderName);
        }

        public string GetCurrentRequestProject()
        {
            return _contextService.GetContextItemValue(Constants.RequestProjectHeaderName);
        }

        public bool IsSameCurrentRequestProject(string projectName)
        {
            return GetCurrentRequestProject() == projectName;
        }
    }
}