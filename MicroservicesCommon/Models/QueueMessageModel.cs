using System.Collections.Generic;

namespace MicroservicesCommon.Models
{
    public class QueueMessageModel<T>
    {
        public T Data { get; set; }
        public IDictionary<string, string> Headers { get; set; }
    }
}