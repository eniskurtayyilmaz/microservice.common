using System.Collections.Generic;

namespace MicroservicesCommon.Models
{
    public class ErrorResponse
    {
        public ErrorResponse()
        {
            Errors = new Dictionary<string, string[]>();
        }
        public int Status { get; set; }
        public string TraceId { get; set; }
        public Dictionary<string,string[]> Errors { get; set; }
    }
}