using System.Collections.Generic;

namespace MicroservicesCommon.Models
{
    public class Pageable<T>
    {
        public IEnumerable<T> Items { get; set; }
        public int Page { get; set; }
        public int ItemsPerPage { get; set; }
        public long PageCount { get; set; }
        public long Total { get; set; }
    }
}