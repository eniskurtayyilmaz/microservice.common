namespace MicroservicesCommon.Models
{
    public abstract class PageFilterRequest
    {
        public PageFilterRequest()
        {
            Size = 10;
            Page = 0;
        }

        public int Size { get; set; }
        public int Page { get; set; }
    }
}