using System.Security.Cryptography;
using System.Text;

namespace MicroservicesCommon.Utils
{
    public class DigestUtils
    {
        public static string Md5(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            var id = Encoding.UTF8.GetBytes(value);
            var hasher = MD5.Create();
            var hash = hasher.ComputeHash(id);
            var builder = new StringBuilder();
            foreach (var item in hash)
            {
                builder.Append(item.ToString("x2"));
            }

            return builder.ToString();
        }
    }
}