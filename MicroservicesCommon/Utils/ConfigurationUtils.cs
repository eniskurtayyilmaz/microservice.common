using System.IO;
using Microsoft.Extensions.Configuration;

namespace MicroservicesCommon.Utils
{
    public static class ConfigurationUtils
    {   
        public static IConfiguration Configuration { get; }

        static ConfigurationUtils()
        {
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
        }
    }
}