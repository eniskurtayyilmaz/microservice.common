# Intro
The repository contains the base components and utils in the microservices. Let's imagine that we are marketplace company like Amazon, Ebay. We have the domains like products, orders, discounts, email senders, file uploading etc. If we want to count the projects, it already became five (5) different projects. 

Let's deep dive and imagine that your five projects/teams need to `healthcheck endpoint` implemantion. Do you want to clone the code for each of the projects? or, two of your projects/teams need to use the `redis cache` and `rabbitmq queue` integration. 

![Example](https://gitlab.com/eniskurtayyilmaz/microservice.common/blob/main/images/drawio.png?raw=true)

Basically, the solution in the repository solves the these issues and blocks the code cloning. The solution just needs to deploy as nuget package internally.

This library contains Global exception handling, Common Domain classes, Business Exception classes, SQL server connection abstraction, Repository Abstraction, Application configuration and some utilities like Date Time helpers, Guid Helpers and Random Helpers.

- Microservices.Common (Base configurations)
- Microservices.BlobServices (Azure integration)
- Microservices.CacheServices (Cache management)
- Microservices.ExcelServices (Excel template or excel files management)
- Microservices.HttpServices (Call the other internal or external services)
- Microservices.NotificationServices (Send email or sms)
- Microservices.RabbitMQ (Queue management)
- Microservices.SqlServer (Repository and SQL Integration)

# Microservices.Common
It is base structure of the Microservice. It helps to provide thus things:
- Base logging
- Base healthcheck
- Base exception handling
- Integration with Consul (like Vault)
- Pass the header values of the request into context
- TraceId - Correlationid management
- Detect user information after internal usages
- Utils, like Clock, Guid, Random

Example usages:
```csharp
///New microservice project Program.cs file
public class Program : Application
{
    public static void Main(string[] args)
    {
        CreateHostBuilderWithExternalConfiguration(args)
            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
            .Build()
            .Run();
    }
}

///New microservice project startup file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddSingleton<IMerchantProductRepository, MerchantProductRepository>();
        services.AddElasticsearch(Configuration);
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        //Internal sets
         app.ApplicationServices.GetService<IMerchantProductStatisticQueueService>()?.Setup();
        ...
    }


    protected override string ApplicationName => nameof(MerchantProductApi);
}
````

# Microservices.BlobServices

It is base structure of the Azure blob integration in the new microservice projects. It helps to provide this things:
- Download the blob/file
- Upload the blob/file
- Delete the blob/file
- Generate upload link (aka SaS Uri, which helps to upload the file for 3rd party consumers via link)
- Generate the download link
- Generate Container Sas Url

Example usages:
appsettings.json file:
```json
{
  "AzureConfig": {
    "ConnectionString": "UseDevelopmentStorage=true"
  },
}
```

```csharp
///New microservice project Startup.cs file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddBlobService(Configuration);
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        ...
    }

    protected override string ApplicationName => nameof(MerchantFileApi);
}
````

# Microservices.CacheServices
It is base structure of the `redis cache integration` in the new microservice projects. It helps to provide this things:
- Get/Set objects with key, or expiration time
- Adds the healthcheck endpoint for redis

Example usages:
appsettings.json file:
```json
{
   "Redis": {
    "Host": "localhost:6379",
    "Password": "",
    "UseSsl": false
  }
}
```
```csharp
///New microservice project startup file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddRedisCacheService(Configuration);
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        ...
    }

    protected override string ApplicationName => nameof(MerchantOrderApi);
}
```

# Microservices.HttpServices
It is base structure of the `http client` integration in the new microservice projects. It helps to provide this things:
- Pass the important values, like traceId and user informations (If it exists) via Header in the request
- Logging the requests, fail requests or unexpected fails

Example usages:
appsettings.json
```json
{
    "BackOfficeApiServiceUrl": "http://localhost:5001"
}
```
```csharp
///New microservice project startup file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddHttpClientService();
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        ...
    }

    protected override string ApplicationName => nameof(MerchantApi);
}

//Service layer
public class BackofficeService
{

    private readonly IHttpClientService _httpClientService;
    public BackofficeService(HttpClientService httpClientService)
    {
       _httpClientService = httpClientService;
    }
    
    public async Task<MerchantCommission> GetMerchantCommission(Guid merchantId)
    {
        var backofficeApiClient = _httpClientService.GetClient("BackOfficeApiServiceUrl");
        var commission = await backofficeApiClient.Request($"/merchant/{merchantId}").GetJsonAsync<MerchantCommission>();
        return commission;
    }
}
```

# Microservices.RabbitMQ
It is base structure of the `rabbitmq` integration in the new microservice projects. It helps to provide this things:
- Pass the important values via Header in the request, like traceId, user informations
- The producer and consumer projects use the project
- Can set the dead letter
- Can get the message

Example usages (in Producer project, like `MerchantApi`):
appsettings.json 
```json
 "RabbitMq": {
    "Address": "localhost",
    "Port": 5672,
    "Username": "admin",
    "Password": "123456"
  },
```
```csharp
///New microservice project Startup.cs file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddRabbitMqConnection(Configuration); 
        services
                .AddSingleton<IMerchantAddressService,
                    MerchantAddressService>();
        services
                .AddSingleton<IMerchantAddressCreatedQueueService,
                    MerchantAddressCreatedQueueService>();
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.ApplicationServices.GetService<IMerchantAddressCreatedQueueService>()?.Setup();
        ...
    }

    protected override string ApplicationName => nameof(MerchantApi);
}

public interface IMerchantAddressCreatedQueueService : IRabbitMqProducerService
{

}
public class MerchantAddressCreatedQueueService : RabbitMqProducerService, IMerchantAddressCreatedQueueService
{
    public MerchantAddressCreatedQueueService(IJsonService jsonService,
        IUserResolverService userResolverService,
        ITraceIdResolverService traceIdResolverService,
        IConnectionFactory connectionFactory) : base(jsonService, userResolverService, traceIdResolverService, connectionFactory)
    {
    }

    protected override string ExchangeName => "merchant-address-created-exchange";
}

public class MerchantAddressService : IMerchantAddressService{
    private readonly IMerchantCreatedQueueService _merchantCreatedQueueService;
    public MerchantAddressService(IMerchantCreatedQueueService merchantCreatedQueueService){
        _merchantCreatedQueueService = merchantCreatedQueueService;
    }
    
    public async Task<Merchant> CreateAsync(MerchantCreateDTO model){
        ...
        var result = await _merchantRepository.AddAsync(merchant);
        if (result > 0) _merchantCreatedQueueService.Publish(merchant);
        return merchant;
    }
}
````

Example usages (in Producer project, like `MerchantUpdateConsumer`):
appsettings.json 
```json
 "RabbitMq": {
    "Address": "localhost",
    "Port": 5672,
    "Username": "admin",
    "Password": "123456"
  },
  "BackOfficeApiServiceUrl": "http://localhost:5001"
```
```csharp
///New microservice project Startup.cs file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddHttpClientService();
        services.AddRabbitMqConnection(Configuration); 
        services.AddSingleton<IMerchantAddressCreatedLogConsumer, MerchantAddressCreatedLogConsumer>();
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.ApplicationServices.GetService<IMerchantCreatedLogConsumer>()?.StartConsume();
        ...
    }

    protected override string ApplicationName => nameof(MerchantUpdateConsumer);
}

public interface IMerchantCreatedLogConsumer : IRabbitMqConsumerService<Merchant>
{
}

public class MerchantCreatedLogConsumer : RabbitMqConsumerService<Merchant>, IMerchantCreatedLogConsumer
{
    private readonly FlurlClient _backofficeApiClient;
    private readonly IJsonService _jsonService;

    public MerchantCreatedLogConsumer(IJsonService jsonService,
        IConnectionFactory connectionFactory,
        ILogger<MerchantCreatedLogConsumer> logger,
        IHttpClientService httpClientService) : base(jsonService, connectionFactory, logger)
    {
        _backofficeApiClient  = _httpClientService.GetClient("BackOfficeApiServiceUrl");
        _jsonService = jsonService;
    }

    protected override string QueueName => "merchant-created-log-queue";

    protected override string ExchangeName => "merchant-created-exchange";

    protected override string DeadLetter => "dead-merchant-created-log-queue";

    public override async Task Consume(QueueMessageModel<Merchant> message)
    {
        //Send the address information to backoffice
        var model = new LogModel
        {
            MerchantId = message.Data.Id,
            Data = _jsonService.Serialize(message.Data),
            LogType = LogType.Merchant_Log
        };

        await _backofficeApiClient.AllowHttpStatus("2**")
            .AddHeader(message.Headers)
            .Request("/log")
            .PostJsonAsync(model);
    }
}
```


# Microservices.SqlServer
It is base structure of the SQL Server integration and repository abstraction in the new microservice projects. It helps to provide this things: 
-  Repository abstraction
-  SQL Server integration abstraction
-  Apply auto migration
-  Adds health check endpoint

Example usages:
```json
{
    "ConnectionStrings": {
        "AppDbContext": "Server=localhost;Database=merchantApi;User=sa;Password=yourStrong(!)Password;Trusted_Connection=False;"
    },
}
```

```csharp
///New microservice project Startup.cs file
public class Startup : BaseStartup
{
    public Startup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureServiceInternal(IServiceCollection services)
    {
        //Internal services
        services.AddSingleton<IMerchantRepository, MerchantRepository>();
        ...
    }

    protected override void ConfigureInternal(IApplicationBuilder app, IWebHostEnvironment env)
    {
        ...
    }

    protected override string ApplicationName => nameof(MerchantApi);
}

public interface IMerchantRepository : IBaseRepository<Merchant>
{
    Task<IEnumerable<Merchant>> GetAllActiveMerchants();
}

public class MerchantRepository : IMerchantRepository
{
    private const string tableName = "merchant";
    private readonly IDatabaseConnectionFactory _factory;

    public MerchantRepository(IDatabaseConnectionFactory factory)
    {
        _factory = factory;
    }
}

public async Task<IEnumerable<Merchant>> GetAllActiveMerchants()
{
    using var conn = await _factory.CreateConnectionAsync();

    var parameters = new
    {
        MerchantStatusType = MerchantStatusType.Active
    };

    return await conn.QueryAsync<Merchant>(
        $"select * from {tableName} where SAPExternalId IS NOT NULL and MerchantStatusType = @MerchantStatusType",
        parameters);
}

```