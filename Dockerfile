FROM beymen.azurecr.io/mplace-base-sdk:v4 AS build

COPY *.sln .
COPY MicroservicesCommon/*.csproj ./MicroservicesCommon/
COPY MicroservicesCommon.ExcelServices/*.csproj ./MicroservicesCommon.ExcelServices/
COPY MicroservicesCommon.BlobServices/*.csproj ./MicroservicesCommon.BlobServices/
COPY MicroservicesCommon.SqlServer/*.csproj ./MicroservicesCommon.SqlServer/
COPY MicroservicesCommon.RabbitMQ/*.csproj ./MicroservicesCommon.RabbitMQ/
COPY MicroservicesCommon.CacheServices/*.csproj ./MicroservicesCommon.CacheServices/
COPY MicroservicesCommon.HttpServices/*.csproj ./MicroservicesCommon.HttpServices/
COPY MicroservicesCommon.NotificationServices/*.csproj ./MicroservicesCommon.NotificationServices/
COPY MicroservicesCommon.Test/. ./MicroservicesCommon.Test/


ARG VERSION
ARG FEED_ACCESSTOKEN
RUN dotnet restore

# Copy and publish app and libraries
COPY MicroservicesCommon/. ./MicroservicesCommon/
COPY MicroservicesCommon.ExcelServices/. ./MicroservicesCommon.ExcelServices/
COPY MicroservicesCommon.BlobServices/. ./MicroservicesCommon.BlobServices/
COPY MicroservicesCommon.SqlServer/. ./MicroservicesCommon.SqlServer/
COPY MicroservicesCommon.RabbitMQ/. ./MicroservicesCommon.RabbitMQ/
COPY MicroservicesCommon.CacheServices/. ./MicroservicesCommon.CacheServices/
COPY MicroservicesCommon.HttpServices/. ./MicroservicesCommon.HttpServices/
COPY MicroservicesCommon.NotificationServices/. ./MicroservicesCommon.NotificationServices/
COPY MicroservicesCommon.Test/. ./MicroservicesCommon.Test/
WORKDIR MicroservicesCommon.Test
RUN dotnet test

WORKDIR ../MicroservicesCommon
RUN dotnet build "MicroservicesCommon.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.ExcelServices
RUN dotnet build "MicroservicesCommon.ExcelServices.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.ExcelServices.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.BlobServices
RUN dotnet build "MicroservicesCommon.BlobServices.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.BlobServices.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.SqlServer
RUN dotnet build "MicroservicesCommon.SqlServer.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.SqlServer.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.RabbitMQ
RUN dotnet build "MicroservicesCommon.RabbitMQ.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.RabbitMQ.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.CacheServices
RUN dotnet build "MicroservicesCommon.CacheServices.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.CacheServices.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.HttpServices
RUN dotnet build "MicroservicesCommon.HttpServices.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.HttpServices.${VERSION}.nupkg

WORKDIR ../MicroservicesCommon.NotificationServices
RUN dotnet build "MicroservicesCommon.NotificationServices.csproj" -c Release -p:PackageVersion=${VERSION}
RUN dotnet nuget push --source "marketplace" --api-key=${FEED_ACCESSTOKEN} bin/Release/MicroservicesCommon.NotificationServices.${VERSION}.nupkg