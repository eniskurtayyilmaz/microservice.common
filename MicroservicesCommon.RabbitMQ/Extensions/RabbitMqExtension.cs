using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace MicroservicesCommon.RabbitMQ.Extensions
{
    public static class RabbitMqExtension
    {
        public static void AddRabbitMqConnection(this IServiceCollection services, IConfiguration configuration)
        {
            if (!configuration.GetSection("RabbitMq").Exists()) return;
            services.AddSingleton<IConnectionFactory>(new ConnectionFactory
            {
                HostName = configuration["RabbitMq:Address"],
                Port = Convert.ToInt32(configuration["RabbitMq:Port"]),
                UserName = configuration["RabbitMq:Username"],
                Password = configuration["RabbitMq:Password"],
                DispatchConsumersAsync = true,
            });
            services.AddHealthChecks().AddRabbitMQ();
        }
    }
}