using System;
using System.Collections.Generic;
using MicroservicesCommon.Services;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace MicroservicesCommon.RabbitMQ.Services
{
    public interface IRabbitMqProducerService
    {
        public void Publish(object data);
        public void Setup();
    }

    public abstract class RabbitMqProducerService : IRabbitMqProducerService
    {
        private readonly IJsonService _jsonService;
        private readonly IUserResolverService _userResolverService;
        private readonly ITraceIdResolverService _traceIdResolverService;
        private readonly IConnectionFactory _factory;
        private IConnection _connection;
        private int _retryCount = 1;
        private const int TryCount = 3;
        protected abstract string ExchangeName { get; }

        protected RabbitMqProducerService(IJsonService jsonService,
            IUserResolverService userResolverService, ITraceIdResolverService traceIdResolverService,
            IConnectionFactory connectionFactory)
        {
            _jsonService = jsonService;
            _userResolverService = userResolverService;
            _traceIdResolverService = traceIdResolverService;
            _factory = connectionFactory;
            _connection = _factory.CreateConnection();
        }

        public void Setup()
        {
            using var channel = _connection.CreateModel();
            const string queueType = "fanout";
            channel.ExchangeDeclare(ExchangeName, type: queueType, durable: true, autoDelete: false);
        }

        public void Publish(object data)
        {
            try
            {
                using var channel = _connection.CreateModel();
                var body = _jsonService.SerializeAsByteArray(data);
                var props = GetQueueProperties(channel);
                props.DeliveryMode = 2;

                channel.BasicPublish(
                    exchange: ExchangeName,
                    routingKey: string.Empty,
                    basicProperties: props,
                    body: body);
                _retryCount = 0;
            }
            catch (Exception ex) when (ex is OperationInterruptedException || ex is BrokerUnreachableException || ex is AlreadyClosedException)
            {
                if (_retryCount == TryCount)
                {
                    throw;
                }
                _connection = _factory.CreateConnection();
                _retryCount++;
                Publish(data);
            }

        }

        private IBasicProperties GetQueueProperties(IModel channel)
        {
            var props = channel.CreateBasicProperties();
            props.Headers = new Dictionary<string, object>
            {
                {Common.Constants.UserIdHeaderName, _userResolverService.GetCurrentUser()},
                {Common.Constants.RoleIdHeaderName, _userResolverService.GetCurrentRole()},
                {Common.Constants.TraceIdHeaderName, _traceIdResolverService.GetTraceId()},
                {Common.Constants.BeymenUserEmailHeaderName, _userResolverService.GetBeymenUserEmail()}
            };
            return props;
        }
    }
}