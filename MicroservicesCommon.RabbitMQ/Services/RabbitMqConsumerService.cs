using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MicroservicesCommon.Models;
using MicroservicesCommon.Services;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace MicroservicesCommon.Services.Queue
{
    public interface IRabbitMqConsumerService<T>
    {
        public void StartConsume();
        public Task Consume(QueueMessageModel<T> message);
    }

    public abstract class RabbitMqConsumerService<T> : IRabbitMqConsumerService<T>, IDisposable
    {
        private readonly IConnectionFactory _connectionFactory;
        private readonly IJsonService _jsonService;
        private readonly ILogger<RabbitMqConsumerService<T>> _logger;
        private IConnection _connection;
        private IModel _channel;
        protected abstract string QueueName { get; }
        protected abstract string ExchangeName { get; }
        protected abstract string DeadLetter { get; }

        protected RabbitMqConsumerService(IJsonService jsonService,
            IConnectionFactory connectionFactory,
            ILogger<RabbitMqConsumerService<T>> logger)
        {
            _connectionFactory = connectionFactory;
            _jsonService = jsonService;
            _logger = logger;
        }

        public void StartConsume()
        {
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            var consumer = new AsyncEventingBasicConsumer(_channel);
            _channel.QueueDeclare(DeadLetter, true, false, false, null);
            _channel.QueueDeclare(queue: QueueName, true, false, false, GetDeadLetterArguments());
            _channel.QueueBind(QueueName, ExchangeName, string.Empty);
            consumer.Received += async (ch, ea) =>
            {
                try
                {
                    using (_logger.BeginScope(ExtractLoggingScopes(ea.BasicProperties)))
                    {
                        var headers = ea.BasicProperties.Headers
                            .Select(p =>
                                new KeyValuePair<string, string>(p.Key, Encoding.UTF8.GetString((byte[]) p.Value)))
                            .ToDictionary(p => p.Key, p => p.Value);
                        var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                        _logger.LogInformation("Consumer Received To Message => {content}", content);
                        await Consume(new QueueMessageModel<T>
                            {
                                Data = _jsonService.DeSerialize<T>(content),
                                Headers = headers
                            }
                        );
                        _channel.BasicAck(ea.DeliveryTag, false);
                        await Task.Yield();
                    }
                }
                catch (Exception e)
                {
                    _channel.BasicNack(ea.DeliveryTag, false, false);
                    _logger.LogError(e, "Message Consumer Error Occurred");
                }
            };
            _channel.BasicConsume(QueueName, false, consumer);
        }

        public abstract Task Consume(QueueMessageModel<T> message);

        public void Dispose()
        {
            try
            {
                _channel.Close();
                _connection.Close();
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Exception occured while disposing channel || connection");
            }
        }

        private Dictionary<string, object> GetDeadLetterArguments()
        {
            return new()
            {
                {"x-dead-letter-exchange", ""},
                {"x-dead-letter-routing-key", DeadLetter}
            };
        }

        private List<KeyValuePair<string, object>> ExtractLoggingScopes(IBasicProperties basicProperties)
        {
            var scopes = new List<KeyValuePair<string, object>>();
            if (basicProperties.Headers.ContainsKey(MicroservicesCommon.Common.Constants.TraceIdHeaderName))
            {
                scopes.Add(new KeyValuePair<string, object>(MicroservicesCommon.Common.Constants.TraceIdHeaderName,
                    Encoding.UTF8.GetString(
                        (byte[]) basicProperties.Headers[MicroservicesCommon.Common.Constants.TraceIdHeaderName])));
            }

            if (basicProperties.Headers.ContainsKey(MicroservicesCommon.Common.Constants.UserIdHeaderName))
            {
                scopes.Add(new KeyValuePair<string, object>(MicroservicesCommon.Common.Constants.UserIdHeaderName,
                    Encoding.UTF8.GetString(
                        (byte[]) basicProperties.Headers[MicroservicesCommon.Common.Constants.UserIdHeaderName])));
            }    
            
            if (basicProperties.Headers.ContainsKey(MicroservicesCommon.Common.Constants.BeymenUserEmailHeaderName))
            {
                scopes.Add(new KeyValuePair<string, object>(MicroservicesCommon.Common.Constants.BeymenUserEmailHeaderName,
                    Encoding.UTF8.GetString(
                        (byte[]) basicProperties.Headers[MicroservicesCommon.Common.Constants.BeymenUserEmailHeaderName])));
            }

            return scopes;
        }
    }
}